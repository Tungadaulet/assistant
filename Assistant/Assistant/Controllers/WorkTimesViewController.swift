//
//  WorkTimesViewController.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 10/30/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import UIKit

class WorkTimesViewController: RegionsViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Время работы"
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredList.workTimes.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        
        cell.textLabel?.textColor = Const.Colors.navBar
        cell.textLabel?.font = UIFont.systemFont(ofSize: 14)
        
        cell.textLabel?.text = filteredList.workTimes[indexPath.row].value
        
        let checked = filteredList.workTimes[indexPath.row].checked == 1
        cell.accessoryView = UIImageView(image: checked ? #imageLiteral(resourceName: "check-mark") : UIImage())
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let checked = filteredList.workTimes[indexPath.row].checked == 1
        filteredList.workTimes[indexPath.row].checked = checked ? 0 : 1
        filteredList.checkedWorkTime = checked ? nil : filteredList.workTimes[indexPath.row].value

        for i in 0..<filteredList.workTimes.count {
            if i != indexPath.row {
                filteredList.workTimes[i].checked = 0
            }
        }
        
        tableView.reloadData()
    }
    
    
}
