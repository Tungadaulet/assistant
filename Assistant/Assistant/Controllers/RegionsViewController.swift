//
//  CityViewController.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 10/30/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import UIKit

class RegionsViewController: UITableViewController {
    
    var filteredList = FilteredList()
    let cellId = "cellId"
    
    let okButton = InfoViewController.createGradientButton(withTitle: "OK")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Выберите район"
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "OK", style: .plain, target: self, action: #selector(okBtnAction))
        
        tableView.separatorInset = .zero
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellId)
    }
    
    func setupFooterButton() {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
        
        view.addSubview(okButton)
        
        okButton.snp.makeConstraints { (make) in
            make.top.equalTo(10)
            make.width.equalTo(view).dividedBy(3)
            make.centerX.equalTo(view)
            make.height.equalTo(40)
        }
        
        tableView.tableFooterView = view
        
        okButton.addTarget(self, action: #selector(okBtnAction), for: .touchUpInside)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        okButton.addGradient()
    }
    
    func okBtnAction() {
        let filteredVenuesVC = navigationController?.viewControllers[0] as! FilteredVenuesViewController
        filteredVenuesVC.filteredList = filteredList
        _ = navigationController?.popToViewController(filteredVenuesVC, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredList.regions.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        
        cell.textLabel?.textColor = Const.Colors.navBar
        cell.textLabel?.font = UIFont.systemFont(ofSize: 14)
        
        cell.textLabel?.text = filteredList.regions[indexPath.row].title
        
        let checked = filteredList.regions[indexPath.row].checked == 1
        cell.accessoryView = UIImageView(image: checked ? #imageLiteral(resourceName: "check-mark") : UIImage())
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let checked = filteredList.regions[indexPath.row].checked == 1
        filteredList.regions[indexPath.row].checked = checked ? 0 : 1
        filteredList.checkedRegion = checked ? nil : filteredList.regions[indexPath.row].title
        
        for i in 0..<filteredList.regions.count {
            if i != indexPath.row {
                filteredList.regions[i].checked = 0
            }
        }
        
        tableView.reloadData()
    }
}
