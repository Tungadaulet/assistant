//
//  HomeViewController.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 9/27/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import UIKit

protocol HomeDelegate {
    
    func didSelectVenue(id: Int, catTitle: String)
}

class HomeViewController: UITableViewController, HomeDelegate, ReviewsDelegate, UISearchControllerDelegate {
    
    var homeList = [HomeList]()
    var banners = [Banner]()
    
    let headerId = "headerId"
    let cellId = "cellId"
    
    lazy var indicatorView: UIView = {
        let aiv = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: aiv.frame.height+16))
        view.addSubview(aiv)
        aiv.snp.makeConstraints { (make) in
            make.centerX.equalTo(view)
            make.bottom.equalTo(0)
        }
        aiv.startAnimating()
        return view
    }()
    
    let aiv = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    
    var searchController = UISearchController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addLeftMenuNavBarItem()
        navigationItem.title = "Главная"
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "search"), style: .plain, target: self, action: #selector(searchAction))
        
        configTableView()
        
        aiv.startAnimating()
        fetchHomeList()
    }
    
    func configTableView() {
        tableView = UITableView(frame: .zero, style: .grouped)
        tableView.backgroundColor = .white
        tableView.estimatedSectionHeaderHeight = 100
        tableView.sectionHeaderHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 300
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.sectionFooterHeight = 0.01
        tableView.separatorStyle = .none
        tableView.backgroundView = aiv
        
        tableView.register(HomeHeader.self, forHeaderFooterViewReuseIdentifier: headerId)
        tableView.register(HomeCell.self, forCellReuseIdentifier: cellId)
        
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(fetchHomeList), for: .valueChanged)
        tableView.addSubview(refreshControl!)
    }
    
    func searchAction() {
        let layout = UICollectionViewFlowLayout()
        let searchVC = HomeSearchViewController(collectionViewLayout: layout)
        searchVC.viewController = self
        searchController = UISearchController(searchResultsController: searchVC)
        searchController.delegate = self
        searchController.searchBar.setValue("Отмена", forKey: "cancelButtonText")
        searchController.searchBar.searchBarStyle = .prominent
        searchController.searchResultsUpdater = searchVC
        searchController.searchBar.barTintColor = Const.Colors.navBar
        searchController.searchBar.tintColor = .white
        searchController.searchBar.placeholder = "Поиск"
        searchController.searchBar.isTranslucent = false
        
        present(searchController, animated: true, completion: nil)
    }
    
    func fetchHomeList() {
        HomeList.fetchHomeList { (list, banners, statusCode) in
            self.homeList = list
            self.banners = banners
            self.tableView.reloadData()
            self.aiv.stopAnimating()
            self.refreshControl?.endRefreshing()
            self.handleError(with: statusCode)
        }
    }
    
    func setReviewsCount(_ count: Int?) {
        
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: headerId) as! HomeHeader
        header.banners = banners
        
        return header
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return homeList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! HomeCell
        
        cell.categoryLabel.text = homeList[indexPath.row].title
        cell.venues = homeList[indexPath.row].venues
        cell.seeAllBtn.tag = indexPath.row
        cell.seeAllBtn.addTarget(self, action: #selector(seeAllAction(sender:)), for: .touchUpInside)
        cell.homeDelegate = self
        
        return cell
    }
    
    func seeAllAction(sender: UIButton) {
        let index = sender.tag
        let catId = homeList[index].id
        let title = homeList[index].title
        
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10)
        let oneCategoryVenuesVC = OneCategoryVenuesVC(collectionViewLayout: layout)
        oneCategoryVenuesVC.catId = catId
        oneCategoryVenuesVC.navigationItem.title = title
        navigationController?.pushViewController(oneCategoryVenuesVC, animated: true)       
    }
    
    func didSelectVenue(id: Int, catTitle: String) {
        let detailVC = VenueDetailViewController()
        detailVC.id = id
        detailVC.navigationItem.title = catTitle
        detailVC.reviewsDelegate = self
        navigationController?.pushViewController(detailVC, animated: true)
    }
}
