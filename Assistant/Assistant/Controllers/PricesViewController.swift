//
//  PricesViewController.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 12/5/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import UIKit
import ExpyTableView

class PricesViewController: VenueMenuViewController {
    
    var filteredList = FilteredList()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Услуги"
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "OK", style: .plain, target: self, action: #selector(okBtnAction))
    }
    
    override func setupViews() {
        view.addSubview(tableView)
        
        tableView.snp.makeConstraints { (make) in
            make.edges.equalTo(UIEdgeInsetsMake(0, 10, 0, 10))
        }
        
        tableView.register(VenueMenuExpandableCell.self, forCellReuseIdentifier: expandableCellId)
        tableView.register(FilterPriceExpandedCell.self, forCellReuseIdentifier: expandedCellId)
    }
    
    func okBtnAction() {
        let filteredVenuesVC = navigationController?.viewControllers[0] as! FilteredVenuesViewController
        filteredVenuesVC.filteredList = filteredList
        _ = navigationController?.popToViewController(filteredVenuesVC, animated: true)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return filteredList.prices.count
    }
    
    override func expandableCell(forSection section: Int, inTableView tableView: ExpyTableView) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: expandableCellId) as! VenueMenuExpandableCell
                
        cell.textLabel?.text = filteredList.prices[section].title
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredList.prices[section].subprices.count+1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: expandedCellId) as! FilterPriceExpandedCell
        
        let title = filteredList.prices[indexPath.section].subprices[indexPath.row-1].title
        let value = filteredList.prices[indexPath.section].subprices[indexPath.row-1].value
        let checked = value != ""
        let price = (value == "" || value == "-1") ? "Цена?" : value! + " тг"
        
        cell.textLabel?.text = title
        cell.priceButton.tag = indexPath.section*1000+indexPath.row
        cell.priceButton.setTitle(price, for: .normal)
        cell.imageView?.image = checked ? #imageLiteral(resourceName: "checked") : #imageLiteral(resourceName: "unchecked")
        
        cell.tag = indexPath.row == filteredList.prices[indexPath.section].subprices.count ? 1 : 0        
        
        cell.priceButton.addTarget(self, action: #selector(priceBtnAction(sender:)), for: .touchUpInside)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row != 0 {
            let checked = filteredList.prices[indexPath.section].subprices[indexPath.row-1].value != ""
            filteredList.prices[indexPath.section].subprices[indexPath.row-1].value = checked ? "" : "-1"
            
            setCheckedPrices(section: indexPath.section)
            
            tableView.reloadData()
        }
    }
    
    func priceBtnAction(sender: UIButton) {
        let section = sender.tag / 1000
        let row = sender.tag % 1000
        let value = filteredList.prices[section].subprices[row-1].value
        
        let alert = UIAlertController(title: "Укажите цену", message: "", preferredStyle: .alert)
        
        alert.addTextField { (textField) in
            textField.keyboardType = .numberPad
            textField.text = value == "-1" ? "" : value
        }
        
        let okAction = UIAlertAction(title: "OK", style: .default) { (Void) in
            let price = alert.textFields![0].text ?? ""
            self.filteredList.prices[section].subprices[row-1].value = price == "" ? "-1" : price
            self.setCheckedPrices(section: section)
            self.tableView.reloadData()
        }
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        alert.addAction(okAction)
        
        present(alert, animated: true, completion: nil)
    }
    
    func setCheckedPrices(section: Int) {
        var checkedPrices = ""
        
        for subprice in filteredList.prices[section].subprices {
            if subprice.value != ""  {
                checkedPrices += "\(subprice.title!),"
            }
        }
        if checkedPrices != "" {
            checkedPrices = checkedPrices.substring(to: checkedPrices.index(checkedPrices.endIndex, offsetBy: -1))
        }
        
        filteredList.checkedPrices = checkedPrices
    }
}
