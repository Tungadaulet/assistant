//
//  AddressInMapVC.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 10/23/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation

class AddressInMapVC: UIViewController, CLLocationManagerDelegate {
    
    var address: String?
    var latitude: Double?
    var longitude: Double?
    
    var mapView = GMSMapView()
    let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupMapAndMarker()
    }
    
    func setupMapAndMarker() {
        let lat = latitude ?? 43.220641
        let lng = longitude ?? 76.85146
        
        let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: lng, zoom: 13.0)
        mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        mapView.settings.compassButton = true
        view = mapView
        
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: lat, longitude: lng)
        marker.title = "Адрес"
        marker.snippet = address
        marker.map = mapView
        marker.icon = #imageLiteral(resourceName: "marker")
    }
    
    
}
