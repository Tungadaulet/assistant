//
//  FilteredVenuesViewController.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 10/26/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import UIKit
import AlamofireDomain
import SVProgressHUD
import CoreLocation

class FilteredVenuesViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout, UISearchControllerDelegate, DropDownMenuDelegate, CLLocationManagerDelegate {
    
    var catId: Int?
    var page = 1
    var availablePage = 0
    var params: Parameters = ["category_id": "",
                  "page": "",
                  "perPage": "10",
                  "information_filter": "",
                  "filter_value_ids": "",
                  "region_ids": "",
                  "work_time_id": "",
                  "search_text": "",
                  "sort_id": "",
                  "lat": "",
                  "lng": "",
                  "price_filter": ""
                ]
    
    var filteredList = FilteredList() {
        didSet {
            collectionView?.reloadData()
        }
    }
    var noLongItems = 0
    
    let emptyHeaderId = "emptyHeaderId"
    let buttonHeaderId = "headerId"
    let filterCellId = "filterCellId"
    let venueCellId = "venueCellId"
    let footerId = "footerId"
    
    let aiv = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    let refreshControl = UIRefreshControl()
    
    var searchController = UISearchController()
    
    let dropDownMenu = DropDownMenu()
    
    let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addLeftMenuNavBarItem()
        let searchItem = UIBarButtonItem(image: #imageLiteral(resourceName: "search"), style: .plain, target: self, action: #selector(searchAction))
        let sortingItem = UIBarButtonItem(image: #imageLiteral(resourceName: "sorting"), style: .plain, target: self, action: #selector(sortingAction))
        navigationItem.rightBarButtonItems = [searchItem, sortingItem]
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)        
        
        collectionView?.backgroundColor = .white
        collectionView?.alwaysBounceVertical = true
        collectionView?.backgroundView = aiv

        collectionView?.register(UICollectionReusableView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: emptyHeaderId)
        collectionView?.register(FilteredVenuesHeader.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: buttonHeaderId)
        collectionView?.register(FilteredVenuesCell.self, forCellWithReuseIdentifier: filterCellId)
        collectionView?.register(HomeSubcatCell.self, forCellWithReuseIdentifier: venueCellId)
        collectionView?.register(IndicatorFooter.self, forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: footerId)
        
        refreshControl.addTarget(self, action: #selector(refreshList), for: .valueChanged)
        collectionView?.addSubview(refreshControl)

        aiv.startAnimating()
        params["category_id"] = "\(catId!)"
        params["page"] = "\(page)"
        fetchFilteredList()
    }
    
    func sortingAction() {
        if filteredList.sortings.count == 0 {
            return
        }
        
        var items = [String]()
        
        for sorting in filteredList.sortings {
            items.append(sorting.value!)
        }
        
        if dropDownMenu.isHidden {
            dropDownMenu.delegate = self
            dropDownMenu.containerView = self.view
            dropDownMenu.items = items
            dropDownMenu.show()
        }
        else {
            dropDownMenu.dismiss()
        }
    }
    
    func searchAction() {
        let layout = UICollectionViewFlowLayout()
        let searchVC = SearchViewController(collectionViewLayout: layout)
        searchVC.catId = catId
        searchVC.viewController = self
        searchController = UISearchController(searchResultsController: searchVC)
        searchController.delegate = self
        searchController.searchBar.setValue("Отмена", forKey: "cancelButtonText")
        searchController.searchBar.searchBarStyle = .prominent
        searchController.searchResultsUpdater = searchVC
        searchController.searchBar.barTintColor = Const.Colors.navBar
        searchController.searchBar.tintColor = .white
        searchController.searchBar.placeholder = "Поиск"
        searchController.searchBar.isTranslucent = false
        
        present(searchController, animated: true, completion: nil)
    }
    
    func refreshList() {
        page = 1
        params["page"] = "\(page)"
        fetchFilteredList()
    }

    func fetchFilteredList() {
        FilteredList.fetchFilteredList(params: params, completionHandler: { (filteredList, statusCode) in
            self.filteredList.regions = filteredList.regions
            self.filteredList.checkedRegion = filteredList.checkedRegion
            self.filteredList.workTimes = filteredList.workTimes
            self.filteredList.checkedWorkTime = filteredList.checkedWorkTime
            self.filteredList.sortings = filteredList.sortings
            self.filteredList.filters = filteredList.filters
            self.filteredList.info = filteredList.info
            self.filteredList.prices = filteredList.prices
            
            let sortParam = self.params["sort_id"] as! String
            if filteredList.sortings.count > 0, let defaultSortId = filteredList.sortings[0].id, sortParam == "" {
                self.params["sort_id"] = "\(defaultSortId)"
            }
            
            if self.page == 1 {
                self.filteredList.venues = filteredList.venues
            }
            else {
                self.filteredList.venues += filteredList.venues
            }
            
            self.noLongItems = 0
            self.collectionView?.reloadData()
            self.aiv.stopAnimating()
            self.refreshControl.endRefreshing()
            SVProgressHUD.self.dismiss()
            self.handleError(with: statusCode)
        })
    }
    
    func didSelectItemAt(index: Int) {
        print("selected drop down item index", index)
        guard let sortId = filteredList.sortings[index].id else {
            return
        }
        let indicator = SVProgressHUD.self
        indicator.setDefaultMaskType(.gradient)
        indicator.show(withStatus: "Идет загрузка...")
        
        // Рядом id = 12
        if sortId == 12 || filteredList.sortings[index].value == "Рядом" {
            besideSortingAction()
        }
        else {
            params["sort_id"] = "\(sortId)"
            fetchFilteredList()
        }
    }
    
    func besideSortingAction() {
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .denied:
                SVProgressHUD.dismiss()
                showAlertWhenGPSDenied()
                
                if let defaultSortId = filteredList.sortings[0].id {
                    self.params["sort_id"] = "\(defaultSortId)"
                    dropDownMenu.selectedItem = 0
                }
                break
            case .authorizedWhenInUse:
                break
            case .notDetermined:
                break
            default:
                break
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("location error:", error.localizedDescription)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let userLocation = locations.last {
            let lat = userLocation.coordinate.latitude
            let lng = userLocation.coordinate.longitude
            
            params["lat"] = lat
            params["lng"] = lng
            params["sort_id"] = "12"
            fetchFilteredList()
            
            for (index, sorting) in filteredList.sortings.enumerated() {
                if sorting.id == 12 || sorting.value == "Рядом" {
                    dropDownMenu.selectedItem = index
                    break
                }
            }
            
            locationManager.stopUpdatingLocation()
        }
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return filteredList.regions.count > 0 ? 2 : 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if indexPath.section == 0 {
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: emptyHeaderId, for: indexPath)
            
            return header
        }
        else {
            if kind == UICollectionElementKindSectionHeader {
                let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: buttonHeaderId, for: indexPath) as! FilteredVenuesHeader
                
                header.showResultBtn.addTarget(self, action: #selector(showResultBtnAction), for: .touchUpInside)
                
                return header
            }
            else {
                let footer = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: footerId, for: indexPath)
                return footer
            }            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        let width = collectionView.frame.width
        let height: CGFloat = section == 0 ? 0 : 40
        
        return CGSize(width: width, height: height)
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            var totalCount = filteredList.filters.count+filteredList.info.count
            totalCount += filteredList.regions.count > 0 ? 1 : 0
            totalCount += filteredList.workTimes.count > 0 ? 1 : 0
            totalCount += filteredList.prices.count > 0 ? 1 : 0
            return totalCount
        }
        else {
            return filteredList.venues.count
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: filterCellId, for: indexPath) as! FilteredVenuesCell
            
            let filters = filteredList.filters
            let info = filteredList.info
            
            if indexPath.item == 0 {                
                cell.label.text = filteredList.checkedRegion == nil ? "Выберите район" : filteredList.checkedRegion
            }
            else if indexPath.item == 1 {
                cell.label.text = filteredList.checkedWorkTime == nil ? "Время работы" : filteredList.checkedWorkTime
            }
            else if indexPath.item > 1 && indexPath.item < filters.count+2 {
                let checkedValues = filters[indexPath.item-2].checkedValues
                cell.label.text = checkedValues == "" ? filters[indexPath.item-2].title : checkedValues
            }
            else if indexPath.item > filters.count+1 && indexPath.item < 2+filters.count+info.count {
                let valueString = info[indexPath.item-filters.count-2].valueString
                cell.label.text = valueString == nil ? info[indexPath.item-filters.count-2].title : valueString
            }
            else {
                cell.label.text = filteredList.checkedPrices == "" ? "Услуги" : filteredList.checkedPrices
            }
            
            return cell
        }
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: venueCellId, for: indexPath) as! HomeSubcatCell
            
            let venues = filteredList.venues
            
            cell.reviewsCountLabel.text = venues[indexPath.item].reviewsCount
            cell.ratingLabel.text = venues[indexPath.item].rating
            cell.discountLabel.text = "-\(venues[indexPath.item].discount!)%"
            cell.subcatLabel.text = venues[indexPath.item].catTitle
            cell.nameLabel.text = venues[indexPath.item].title
            
            let url = URL(string: venues[indexPath.item].image!)
            cell.imageView.kf.setImage(with: url, options: [.transition(.fade(0.2))])

            cell.discountImageView.isHidden = venues[indexPath.item].discount == 0
            cell.discountLabel.isHidden = cell.discountImageView.isHidden
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.section == 0 {
            var width: CGFloat
            
            let filters = filteredList.filters
            
            if indexPath.item < 2 {
                width = collectionView.frame.width-16
            }
            else {
                var isLong = false
                
                if indexPath.item > 1 && indexPath.item < filters.count+2 {
                    isLong = filters[indexPath.item-2].isLong
                }
                
                if isLong {
                    width = collectionView.frame.width-16
                }
                else {
                    let itemsCount = collectionView.numberOfItems(inSection: 0)
                    if noLongItems == 0 {
                        noLongItems = itemsCount-indexPath.item
                    }
                    
                    if noLongItems % 2 == 1 && indexPath.item == itemsCount-noLongItems {
                        width = collectionView.frame.width-16
                    }
                    else {
                        width = (collectionView.frame.width-24) / 2
                    }
                }
            }
            
            return CGSize(width: width, height: 40)
        }
        else {
            let width = (collectionView.frame.width-30)/2
            let height = width*0.7
            
            return CGSize(width: width, height: height)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if section == 0 {
            return UIEdgeInsetsMake(10, 8, 10, 8)
        }
        else {
            return UIEdgeInsetsMake(10, 10, 10, 10)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return section == 0 ? 8 : 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return section == 0 ? 8 : 10
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let filters = filteredList.filters
        let info = filteredList.info
        
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                let regionsVC = RegionsViewController()
                regionsVC.filteredList = filteredList
                navigationController?.pushViewController(regionsVC, animated: true)
            }
            else if indexPath.row == 1 {
                let workTimesVC = WorkTimesViewController()
                workTimesVC.filteredList = filteredList
                navigationController?.pushViewController(workTimesVC, animated: true)
            }
            else if indexPath.item > 1 && indexPath.item < filters.count+2 {
                let filtersVC = FiltersViewController()
                filtersVC.filteredList = filteredList
                let index = indexPath.item-2
                filtersVC.filterIndex = index
                filtersVC.navigationItem.title = filteredList.filters[index].title
                navigationController?.pushViewController(filtersVC, animated: true)
            }
            else if indexPath.item > filters.count+1 && indexPath.item < 2+filters.count+info.count {
                let filterInfoVC = FilterInfoViewController()
                filterInfoVC.filteredList = filteredList
                let index = indexPath.item-filteredList.filters.count-2
                filterInfoVC.infoIndex = index
                filterInfoVC.navigationItem.title = filteredList.info[index].title
                navigationController?.pushViewController(filterInfoVC, animated: true)
            }
            else {
                let pricesVC = PricesViewController()
                pricesVC.filteredList = filteredList
                navigationController?.pushViewController(pricesVC, animated: true)
            }
        }
        else {
            let detailVC = VenueDetailViewController()
            detailVC.id = filteredList.venues[indexPath.item].id
            detailVC.navigationItem.title = filteredList.venues[indexPath.item].catTitle
            navigationController?.pushViewController(detailVC, animated: true)
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        let lastItem = collectionView.numberOfItems(inSection: 1)-1
        let noRemainder = filteredList.venues.count % 10 == 0
        
        if indexPath.item == lastItem && noRemainder {
            page += 1
            params["page"] = "\(page)"
            fetchFilteredList()
        }
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
//
//        var height: CGFloat
//        height = (catVenues.count != totalCount) ? 50 : 0
//
//        return CGSize(width: collectionView.frame.width, height: height)
//    }
    
    func showResultBtnAction() {
        let indicator = SVProgressHUD.self
        indicator.setDefaultMaskType(.gradient)
        indicator.show(withStatus: "Идет загрузка...")
        
        var regionIds = [Int]()
        var workTimeId = ""
        var filterValueIds = [Int]()
        var infoFilter = ""
        var priceFilter = ""
        
        for region in filteredList.regions {
            if region.checked == 1 {
                guard let id = region.id else { return }
                regionIds.append(id)
                break
            }
        }
        
        for workTime in filteredList.workTimes {
            if workTime.checked == 1 {
                guard let id = workTime.id else { return }
                workTimeId = "\(id)"
                break
            }
        }
        
        for filter in filteredList.filters {
            for value in filter.values {
                if value.checked == 1 {
                    guard let id = value.id else { return }
                    filterValueIds.append(id)
                }
            }
        }
        
        var infoIds = [Int]()
        var infoValues = [Int]()
        
        for info in filteredList.info {
            if info.value != 0 {
                guard let id = info.id else { return }
                guard let value = info.value else { return }
                infoIds.append(id)
                infoValues.append(value)
            }
        }
        
        if infoIds.count > 0 {
            infoFilter = "{\"id\":\(infoIds),\"value\":\(infoValues)}"
        }
        
        var priceIds = [Int]()
        var priceValues = [String]()
        
        for price in filteredList.prices {
            for subprice in price.subprices {
                if subprice.value != "" {
                    guard let id = subprice.id else { return }
                    guard let value = subprice.value else { return }
                    priceIds.append(id)
                    priceValues.append(value)
                }
            }
        }
        
        if priceIds.count > 0 {
            priceFilter = "{\"id\":\(priceIds),\"value\":\(priceValues)}"
            print("price filter", priceFilter)
        }
        
        params["region_ids"] = "\(regionIds)"
        params["work_time_id"] = workTimeId
        params["filter_value_ids"] = "\(filterValueIds)"
        params["information_filter"] = infoFilter
        params["price_filter"] = priceFilter
        page = 1
        params["page"] = "\(page)"
        fetchFilteredList()
    }
}


