//
//  VenueMenuViewController.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 10/24/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import UIKit
import ExpyTableView

class VenueMenuViewController: UIViewController, ExpyTableViewDataSource, ExpyTableViewDelegate {
    
    var menu = VenueMenu()
    var serviceCharge: Int?
    var discount: Int?
    
    let expandableCellId = "expandableCellId"
    let expandedCellId = "expandedCellId"
    
    lazy var tableView: ExpyTableView = {
        let tv = ExpyTableView(frame: .zero, style: .grouped)
        tv.dataSource = self
        tv.delegate = self
        tv.backgroundColor = .clear
        tv.separatorStyle = .none
        tv.sectionHeaderHeight = 5
        tv.sectionFooterHeight = 5
        tv.contentInset = UIEdgeInsetsMake(-22, 0, 0, 0)
        tv.showsVerticalScrollIndicator = false
        tv.showsHorizontalScrollIndicator = false
        return tv
    }()
    
    let whiteView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.shadowColor = UIColor.lightGray.cgColor
        view.layer.shadowOffset = .zero
        view.layer.shadowRadius = 10
        view.layer.shadowOpacity = 0.8
        return view
    }()
    
    let serviceLabel = VenueMenuViewController.createLabel(withText: "Обслуживание:")
    let discountLabel = VenueMenuViewController.createLabel(withText: "Скидка:")
    let sumLabel = VenueMenuViewController.createLabel(withText: "Сумма:")
    
    let serviceValueLabel = VenueMenuViewController.createLabel(isValueLabel: true)
    let discountValueLabel = VenueMenuViewController.createLabel(isValueLabel: true)
    let sumValueLabel = VenueMenuViewController.createLabel(isValueLabel: true)
    
    let lineView: UIView = {
        let view = UIView()
        view.backgroundColor = Const.Colors.navBar
        return view
    }()
    
    let countButton = InfoViewController.createGradientButton(withTitle: "Посчитать")
    
    static func createLabel(withText text: String = "", isValueLabel: Bool = false) -> UILabel {
        let label = UILabel()
        label.text = text
        label.textColor = Const.Colors.navBar
        if isValueLabel {
            label.font = UIFont.boldSystemFont(ofSize: 15)
            label.textAlignment = .right
        }
        else {
            label.font = UIFont.systemFont(ofSize: 15)
        }
        return label
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        setupViews()
        
        if let service = serviceCharge, let discount = discount {
            serviceValueLabel.text = "+\(service)%"
            discountValueLabel.text = "-\(discount)%"
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        countButton.addGradient()
    }
    
    func setupViews() {
        view.addSubview(whiteView)
        view.addSubview(countButton)
        view.addSubview(sumLabel)
        view.addSubview(sumValueLabel)
        view.addSubview(lineView)
        view.addSubview(discountLabel)
        view.addSubview(discountValueLabel)
        view.addSubview(serviceLabel)
        view.addSubview(serviceValueLabel)
        view.addSubview(tableView)
        
        countButton.snp.makeConstraints { (make) in
            make.left.equalTo(60)
            make.right.equalTo(-60)
            make.bottom.equalTo(-12)
            make.height.equalTo(40)
        }
        
        sumLabel.snp.makeConstraints { (make) in
            make.left.equalTo(20)
            make.bottom.equalTo(countButton.snp.top).offset(-16)
        }
        
        sumValueLabel.snp.makeConstraints { (make) in
            make.top.equalTo(sumLabel)
            make.left.equalTo(sumLabel.snp.right).offset(10)
            make.right.equalTo(-20)
        }
        
        lineView.snp.makeConstraints { (make) in
            make.left.equalTo(20)
            make.bottom.equalTo(sumLabel.snp.top).offset(-16)
            make.right.equalTo(-20)
            make.height.equalTo(1)
        }
        
        discountLabel.snp.makeConstraints { (make) in
            make.left.equalTo(20)
            make.bottom.equalTo(lineView.snp.top).offset(-12)
        }
        
        discountValueLabel.snp.makeConstraints { (make) in
            make.top.equalTo(discountLabel)
            make.left.equalTo(discountLabel.snp.right).offset(10)
            make.right.equalTo(sumValueLabel)
        }
        
        serviceLabel.snp.makeConstraints { (make) in
            make.left.equalTo(20)
            make.bottom.equalTo(discountLabel.snp.top).offset(-8)
        }
        
        serviceValueLabel.snp.makeConstraints { (make) in
            make.top.equalTo(serviceLabel)
            make.left.equalTo(serviceLabel.snp.right).offset(10)
            make.right.equalTo(sumValueLabel)
        }
        
        whiteView.snp.makeConstraints { (make) in
            make.left.bottom.right.equalTo(0)
            make.top.equalTo(serviceLabel.snp.top).offset(-12)
        }
        
        tableView.snp.makeConstraints { (make) in
            if #available(iOS 11, *) {
                make.top.equalTo(0)
            }
            else {
                make.top.equalTo(64)
            }
            make.left.equalTo(20)
            make.right.equalTo(-20)
            make.bottom.equalTo(whiteView.snp.top)
        }
        
        tableView.register(VenueMenuExpandableCell.self, forCellReuseIdentifier: expandableCellId)
        tableView.register(VenueMenuExpandedCell.self, forCellReuseIdentifier: expandedCellId)
        
        countButton.addTarget(self, action: #selector(countBtnAction), for: .touchUpInside)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return menu.prices.count
    }
    
    func expandableCell(forSection section: Int, inTableView tableView: ExpyTableView) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: expandableCellId) as! VenueMenuExpandableCell
        
        cell.textLabel?.text = menu.prices[section].title
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menu.prices[section].values.count+1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: expandedCellId) as! VenueMenuExpandedCell
        
        cell.priceLabel.text = menu.prices[indexPath.section].values[indexPath.row-1].price
        cell.titleLabel.text = menu.prices[indexPath.section].values[indexPath.row-1].title
        
        cell.tag = indexPath.row == menu.prices[indexPath.section].values.count ? 1 : 0
        
        let isChecked = menu.prices[indexPath.section].values[indexPath.row-1].checked
        cell.imageView?.image = isChecked ? #imageLiteral(resourceName: "checked-square") : #imageLiteral(resourceName: "unchecked-square")
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            return
        }
        
        let isChecked = menu.prices[indexPath.section].values[indexPath.row-1].checked
        menu.prices[indexPath.section].values[indexPath.row-1].checked = !isChecked
        
        tableView.reloadData()
    }
    
    func countBtnAction() {
        var sum: Double = 0
        
        for price in menu.prices {
            for value in price.values {
                if value.checked {
                    if let priceStr = value.price?.replacingOccurrences(of: " тг", with: ""), let price = Double(priceStr) {
                        sum += price
                    }
                }
            }
        }
        if let service = serviceCharge, let discount = discount {
            sum -= Double(discount)/100*sum
            sum += Double(service)/100*sum
        }
        
        sumValueLabel.text = "\(Int(sum)) тг"
    }
    
}
