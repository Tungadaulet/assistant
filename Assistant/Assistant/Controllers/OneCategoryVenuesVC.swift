//
//  OneCatVenuesViewController.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 10/19/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import UIKit

class OneCategoryVenuesVC: UICollectionViewController, UICollectionViewDelegateFlowLayout, UISearchControllerDelegate {
    
    var catId: Int?
    var catVenues = [CategoryVenue]()
    var page = 1
    var totalCount = 0
    
    let cellId = "cellId"
    let footerId = "footerId"
    
    let aiv = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    let refreshControl = UIRefreshControl()
    
    var searchController = UISearchController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "search"), style: .plain, target: self, action: #selector(searchAction))
        
        collectionView?.alwaysBounceVertical = true
        collectionView?.backgroundColor = .white
        collectionView?.backgroundView = aiv

        collectionView?.register(HomeSubcatCell.self, forCellWithReuseIdentifier: cellId)
        collectionView?.register(IndicatorFooter.self, forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: footerId)
        
        refreshControl.addTarget(self, action: #selector(refreshList), for: .valueChanged)
        collectionView?.addSubview(refreshControl)
        
        aiv.startAnimating()
        fetchCategoryVenues()
    }
    
    func searchAction() {
        let layout = UICollectionViewFlowLayout()
        let searchVC = SearchInCategoryVC(collectionViewLayout: layout)
        searchVC.catId = catId
        searchVC.viewController = self
        searchController = UISearchController(searchResultsController: searchVC)
        searchController.delegate = self
        searchController.searchBar.setValue("Отмена", forKey: "cancelButtonText")
        searchController.searchBar.searchBarStyle = .prominent
        searchController.searchResultsUpdater = searchVC
        searchController.searchBar.barTintColor = Const.Colors.navBar
        searchController.searchBar.tintColor = .white
        searchController.searchBar.placeholder = "Поиск"
        searchController.searchBar.isTranslucent = false
        
        present(searchController, animated: true, completion: nil)
    }
    
    func refreshList() {
        page = 1
        fetchCategoryVenues()
    }
    
    func fetchCategoryVenues() {
        CategoryVenue.fetchVenuesByCat(id: catId!, page: page) { (venues, count, statusCode) in
            self.totalCount = count
            if self.page == 1 {
                self.catVenues = venues
            }
            else {
                self.catVenues += venues
            }
            self.collectionView?.reloadData()
            self.aiv.stopAnimating()
            self.refreshControl.endRefreshing()
            self.handleError(with: statusCode)
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return catVenues.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! HomeSubcatCell
        
        cell.reviewsCountLabel.text = catVenues[indexPath.item].reviewsCount
        cell.ratingLabel.text = catVenues[indexPath.item].rating
        cell.discountLabel.text = "-\(catVenues[indexPath.item].discount!)%"
        cell.subcatLabel.text = catVenues[indexPath.item].catTitle
        cell.nameLabel.text = catVenues[indexPath.item].title
        
        let url = URL(string: catVenues[indexPath.item].image!)
        cell.imageView.kf.setImage(with: url, options: [.transition(.fade(0.2))])

        cell.discountImageView.isHidden = catVenues[indexPath.item].discount == 0
        cell.discountLabel.isHidden = cell.discountImageView.isHidden
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let detailVC = VenueDetailViewController()
        detailVC.id = catVenues[indexPath.item].id
        detailVC.navigationItem.title = catVenues[indexPath.item].catTitle
        navigationController?.pushViewController(detailVC, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.frame.width-30)/2
        let height = width*0.7
        
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let lastItem = collectionView.numberOfItems(inSection: 0)-1

        if indexPath.item == lastItem && catVenues.count != totalCount {
            page += 1
            fetchCategoryVenues()
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let footer = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: footerId, for: indexPath)
        return footer
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        
        var height: CGFloat
        height = (catVenues.count != totalCount) ? 50 : 0
        
        return CGSize(width: collectionView.frame.width, height: height)
    }
}
