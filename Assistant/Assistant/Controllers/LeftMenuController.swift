//
//  LeftMenuController.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 9/27/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import UIKit
import ExpyTableView
import SafariServices

class LeftMenuController: UIViewController, ExpyTableViewDataSource, ExpyTableViewDelegate, SFSafariViewControllerDelegate {
    
    var categories = [Category]()
    
    lazy var tableView: ExpyTableView = {
        let tv = ExpyTableView()
        tv.dataSource = self
        tv.delegate = self
        tv.backgroundColor = Const.Colors.navBar
        tv.separatorStyle = .none
        return tv
    }()
    
    let headerId = "headerId"
    let expandableCellId = "expandableCellId"
    let expandedCellId = "expandedCellId"
    
    let bottomView: UIView = {
        let view = UIView()
        view.backgroundColor = Const.Colors.menu
        return view
    }()
    
    let phoneButton = LeftMenuController.createBottomButton(withImage: #imageLiteral(resourceName: "phone-call"))
    let instagramButton = LeftMenuController.createBottomButton(withImage: #imageLiteral(resourceName: "instagram"))
    
    static func createBottomButton(withImage image: UIImage) -> UIButton {
        let button = UIButton()
        button.setImage(image, for: .normal)
        return button
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = Const.Colors.navBar

        setupViews()
        
        fetchCategories()
    }
    
    func setupViews() {
        view.addSubview(tableView)
        view.addSubview(bottomView)
        bottomView.addSubview(phoneButton)
        bottomView.addSubview(instagramButton)
        
        bottomView.snp.makeConstraints { (make) in
            make.left.bottom.right.equalTo(0)
            make.height.equalTo(60)
        }
        
        phoneButton.snp.makeConstraints { (make) in
            make.right.equalTo(view.snp.centerX).offset(-20)
            make.centerY.equalTo(bottomView)
        }
        
        instagramButton.snp.makeConstraints { (make) in
            make.left.equalTo(view.snp.centerX).offset(20)
            make.centerY.equalTo(bottomView)
        }
        
        tableView.snp.makeConstraints { (make) in
            make.edges.equalTo(UIEdgeInsetsMake(20, 0, 0, 0))
        }
        
        tableView.register(MenuHeaderCell.self, forCellReuseIdentifier: headerId)
        tableView.register(ExpandableCell.self, forCellReuseIdentifier: expandableCellId)
        tableView.register(ExpandedCell.self, forCellReuseIdentifier: expandedCellId)
        
        phoneButton.addTarget(self, action: #selector(phoneBtnAction), for: .touchUpInside)
        instagramButton.addTarget(self, action: #selector(instagramBtnAction), for: .touchUpInside)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let bottomViewHeight = bottomView.frame.height
        tableView.contentInset = UIEdgeInsetsMake(0, 0, bottomViewHeight, 0)
    }
    
    func phoneBtnAction() {
        let phone = "+77025859316"
        if let url = URL(string: "tel://\(phone)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    func instagramBtnAction() {
        let instagramWeb = URL(string: "http://instagram.com/midas.almaty")!
        let instagramApp = URL(string: "instagram://user?username=midas.almaty")!
        
        if UIApplication.shared.canOpenURL(instagramApp) {
            openSafari(url: instagramApp)
        }
        else {
            if #available(iOS 9.0, *) {
                let svc = SFSafariViewController(url: instagramWeb)
                svc.delegate = self
                guard let window = UIApplication.shared.keyWindow, let rootVC = window.rootViewController else {
                    openSafari(url: instagramWeb)
                    return
                }
                UIApplication.shared.statusBarStyle = .default
                rootVC.present(svc, animated: true, completion: nil)
            } else {
                openSafari(url: instagramWeb)
            }
        }
    }
    
    func openSafari(url: URL) {
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    @available(iOS 9.0, *)
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    func fetchCategories() {
        Category.fetchCategories { (cats, statusCode) in
            self.categories = cats
            self.tableView.reloadData()
            self.handleError(with: statusCode)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return categories.count + 1
    }
    
    func expandableCell(forSection section: Int, inTableView tableView: ExpyTableView) -> UITableViewCell {
        if section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: headerId) as! MenuHeaderCell
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: expandableCellId) as! ExpandableCell
            
            cell.textLabel?.text = categories[section-1].title
            let id = categories[section-1].id
            cell.imageView?.image = UIImage(named: "cat-\(id!)")
            
            let subcatsCount = categories[section-1].subcats.count
            cell.accessoryView = subcatsCount > 0 ? cell.arrowImageView : UIView()
            cell.selectionStyle = subcatsCount > 0 ? .none : .default
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? 1 : categories[section-1].subcats.count+1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            return UITableViewCell()
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: expandedCellId) as! ExpandedCell
            
            cell.textLabel?.text = categories[indexPath.section-1].subcats[indexPath.row-1].title
            cell.countLabel.text = categories[indexPath.section-1].subcats[indexPath.row-1].venuesCount
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section != 0 && indexPath.row == 0 {
            return 60
        }
        else {
            return UITableViewAutomaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sections = tableView.numberOfSections

        for i in 0..<sections {
            if i != indexPath.section {
                self.tableView.collapse(i)
            }
        }
        
        tableView.deselectRow(at: indexPath, animated: false)
                
        if indexPath.row != 0 {
            let id = categories[indexPath.section-1].subcats[indexPath.row-1].id
            let title = categories[indexPath.section-1].subcats[indexPath.row-1].title
            
            let layout = UICollectionViewFlowLayout()
            let filteredVenuesVC = FilteredVenuesViewController(collectionViewLayout: layout)
            filteredVenuesVC.navigationItem.title = title
            filteredVenuesVC.catId = id
            openViewController(vc: filteredVenuesVC)
        }
        else if indexPath.section == 0 && indexPath.row == 0 {
            openViewController(vc: HomeViewController())
        }
        else if indexPath.section == categories.count {
            openViewController(vc: NotificationsViewController())
        }
    }
    
    func openViewController(vc: UIViewController) {
        let centerNavController = UINavigationController(rootViewController: vc)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let menuController = appDelegate.menuController!
        
        menuController.centerViewController = centerNavController
        menuController.toggle(.left, animated: true, completion: nil)
    }
}



    
