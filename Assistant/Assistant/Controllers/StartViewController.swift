//
//  StartViewController.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 9/26/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import UIKit
import SnapKit

class StartViewController: UIViewController {
    
    let bgImageView = UIImageView(image: #imageLiteral(resourceName: "start"))
    
    let maskView: UIView = {
        let view = UIView()
        view.backgroundColor = Const.Colors.navBar.withAlphaComponent(0.8)
        return view
    }()
    
    let appNameLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        let attributedString = NSMutableAttributedString(string: "ASSISTANT ", attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 22)])
        attributedString.append(NSAttributedString(string: "APP", attributes: [NSFontAttributeName: UIFont.boldSystemFont(ofSize: 22)]))
        label.attributedText = attributedString
        label.textAlignment = .center
        return label
    }()
    
    let whiteView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.rgb(203, green: 203, blue: 210)
        view.layer.cornerRadius = 2
        return view
    }()
    
    let aboutTextView: UITextView = {
        let tv = UITextView()
        tv.textColor = .white
        tv.font = UIFont.systemFont(ofSize: 14)
        tv.isSelectable = false
        tv.isEditable = false
        tv.isScrollEnabled = false
        tv.backgroundColor = .clear
        tv.text = "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
        tv.textAlignment = .center
        tv.textContainerInset = UIEdgeInsetsMake(0, 0, 0, 0)
        return tv
    }()
    
    let startBtn = StartViewController.createWhiteBorderButton(withTitle: "Начать работу")
    
    static func createWhiteBorderButton(withTitle title: String) -> UIButton {
        let button = UIButton(type: .system)
        button.setTitle(title, for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .clear
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.white.cgColor
        button.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        button.layer.cornerRadius = 20
        return button
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
    }
    
    func setupViews() {
        view.addSubview(bgImageView)
//        view.addSubview(maskView)
//        view.addSubview(appNameLabel)
//        view.addSubview(whiteView)
//        view.addSubview(aboutTextView)
        view.addSubview(startBtn)
        
        bgImageView.snp.makeConstraints { (make) in
            make.edges.equalTo(0)
        }

//        aboutTextView.snp.makeConstraints { (make) in
//            make.centerY.equalTo(view)
//            make.left.equalTo(20)
//            make.right.equalTo(-20)
//        }
//        
//        whiteView.snp.makeConstraints { (make) in
//            make.bottom.equalTo(aboutTextView.snp.top).offset(-6)
//            make.centerX.equalTo(view)
//            make.width.equalTo(view).multipliedBy(0.125)
//            make.height.equalTo(4)
//        }
//        
//        appNameLabel.snp.makeConstraints { (make) in
//            make.bottom.equalTo(whiteView.snp.top).offset(-6)
//            make.left.right.equalTo(0)
//        }
//        
//        maskView.snp.makeConstraints { (make) in
//            make.top.equalTo(appNameLabel.snp.top).offset(-25)
//            make.left.right.equalTo(0)
//            make.bottom.equalTo(aboutTextView.snp.bottom).offset(25)
//        }
        
        startBtn.snp.makeConstraints { (make) in
            make.height.equalTo(40)
            make.left.equalTo(25)
            make.right.equalTo(-25)
            make.bottom.equalTo(-40)
        }
        
        startBtn.addTarget(self, action: #selector(startBtnAction), for: .touchUpInside)
    }
    
    func startBtnAction() {
        present(LoginViewController(), animated: true, completion: nil)
    }
    
}








