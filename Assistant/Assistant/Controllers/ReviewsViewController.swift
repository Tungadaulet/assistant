//
//  ReviewsViewController.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 10/24/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import UIKit
import AlamofireDomain
import SwiftyJSON
import SVProgressHUD

protocol ReviewsDelegate {
    
    func setReviewsCount(_ count: Int?)
}

class ReviewsViewController: UITableViewController, UITextViewDelegate {
    
    var venueId: Int?    
    var reviews = [Review]()
    var page = 1
    var count: Int?
    
    var delegate: ReviewsDelegate?
    
    let headerId = "headerId"
    let cellId = "cellId"
    let footerId = "footerId"
    
    let aiv = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    let footerAiv = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    
    let moreReviewsBtn = InfoViewController.createGradientButton(withTitle: " Еще отзывы", image: #imageLiteral(resourceName: "re"))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configTableView()
        
        aiv.startAnimating()
        fetchReviews()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        moreReviewsBtn.addGradient()
    }
    
    func configTableView() {
        tableView = UITableView(frame: .zero, style: .grouped)
        tableView.estimatedSectionHeaderHeight = 100
        tableView.sectionHeaderHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.sectionFooterHeight = 5
        tableView.backgroundColor = .white
        tableView.backgroundView = aiv
        
        tableView.register(ReviewsHeader.self, forHeaderFooterViewReuseIdentifier: headerId)
        tableView.register(ReviewsCell.self, forCellReuseIdentifier: cellId)
       // tableView.register(ReviewsFooter.self, forHeaderFooterViewReuseIdentifier: footerId)
        
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(refreshList), for: .valueChanged)
        tableView.addSubview(refreshControl!)
    }
    
    func refreshList() {
        page = 1
        fetchReviews()
    }
    
    func fetchReviews() {
        Review.fetchReviewsOfVenue(id: venueId!, page: page) { (reviews, count, statusCode) in
            self.count = count
            if self.page == 1 {
                self.reviews = reviews
            }
            else {
                self.reviews += reviews
            }
            if self.count != self.reviews.count {
                self.setupFooterWithButton()
            }
            else {
                self.tableView.tableFooterView = UIView()
            }
            
            self.tableView.reloadData()
            self.aiv.stopAnimating()
            self.refreshControl?.endRefreshing()
            self.handleError(with: statusCode)
        }
    }
    
    func setupFooterWithButton() {
        let footer = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 60))
        footer.addSubview(moreReviewsBtn)
        
        moreReviewsBtn.snp.makeConstraints { (make) in
            make.top.equalTo(10)
            make.left.equalTo(60)
            make.right.equalTo(-60)
            make.height.equalTo(40)
        }
        
        moreReviewsBtn.addTarget(self, action: #selector(fetchMoreReviews), for: .touchUpInside)
        tableView.tableFooterView = footer
    }
    
    func fetchMoreReviews(sender: UIButton) {
        footerAiv.startAnimating()
        tableView.tableFooterView = footerAiv
        page += 1
        fetchReviews()
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: headerId) as! ReviewsHeader
        
        let attributedString = NSMutableAttributedString(string: "Отзывы: ", attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 14)])
        if let reviewsCount = count {
            attributedString.append(NSAttributedString(string: "\(reviewsCount)", attributes:[NSFontAttributeName: UIFont.boldSystemFont(ofSize: 14)]))
        }
        header.reviewsCountLabel.attributedText = attributedString
        header.textView.delegate = self
        
        header.sendButton.addTarget(self, action: #selector(sendReview), for: .touchUpInside)
        
        return header
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reviews.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! ReviewsCell
        
        cell.dateLabel.text = reviews[indexPath.row].date
        cell.nameLabel.text = reviews[indexPath.row].name
        cell.reviewTextView.text = reviews[indexPath.row].text
        
        return cell
    }
    
    func textViewDidChange(_ textView: UITextView) {
        tableView.beginUpdates()
        tableView.endUpdates()
    }
    
    func sendReview(sender: UIButton) {
        let header = sender.superview as! ReviewsHeader
        let text = header.textView.text!
        
        if text.isEmpty {
            return
        }
        view.endEditing(true)
        
        let indicator = SVProgressHUD.self
        indicator.setDefaultMaskType(.gradient)
        indicator.show(withStatus: "Идет загрузка...")
        
        let URL = Const.URL.addReview
        
        let token = userDefaults.getAccessToken()
        let headers = ["accessToken": token]
        let params = ["venue_id": "\(venueId!)", "text": text]
        
        AlamofireDomain.request(URL, method: .post, parameters: params, headers: headers).responseJSON { (response) in
            indicator.dismiss()
            
            if response.response?.statusCode == 201 {
                if let value = response.result.value {
                    let json = JSON(value)
                    let review = Review()
                    review.text = json["result"]["text"].stringValue
                    review.name = json["result"]["name"].stringValue
                    review.date = json["result"]["date"].stringValue
                    header.textView.text = ""
                    
                    self.reviews.insert(review, at: 0)
                    if let reviewCount = self.count {
                        self.count = reviewCount + 1
                        if self.count != self.reviews.count {
                            self.reviews.remove(at: self.reviews.count-1)
                        }
                    }
                    self.tableView.reloadData()
                    
                    self.delegate?.setReviewsCount(self.count)
                    
                    indicator.showSuccess(withStatus: "Успешно!")
                    indicator.dismiss(withDelay: 1)
                }
            }
        }
    }
}
