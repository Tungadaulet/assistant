//
//  SearchController.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 11/3/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import UIKit
import AlamofireDomain

class SearchViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout, UISearchResultsUpdating {

    var viewController: UIViewController?
    var catId: Int?
    var page = 1
    var params: Parameters = ["category_id": "",
                              "page": "",
                              "perPage": "10",
                              "search_text": ""
    ]
    var venues = [Venue]()
    
    let aiv = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    
    let cellId = "cellId"
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        collectionView?.backgroundColor = .white
        collectionView?.backgroundView = aiv
        
        collectionView?.register(HomeSubcatCell.self, forCellWithReuseIdentifier: cellId)
        
        aiv.startAnimating()
        if let catId = catId {
            params["category_id"] = "\(catId)"
        }
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        searchController.searchResultsController?.view.isHidden = false

        let text = searchController.searchBar.text
        params["search_text"] = text
        page = 1
        params["page"] = "\(page)"
        searchVenues()
    }
    
    func searchVenues() {
        FilteredList.fetchFilteredList(params: params) { (list, statusCode) in
            if self.page == 1 {
                self.venues = list.venues
            }
            else {
                self.venues += list.venues
            }
            self.collectionView?.reloadData()
            self.aiv.stopAnimating()
            self.handleError(with: statusCode)
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return venues.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! HomeSubcatCell
        
        cell.reviewsCountLabel.text = venues[indexPath.item].reviewsCount
        cell.ratingLabel.text = venues[indexPath.item].rating
        cell.discountLabel.text = "-\(venues[indexPath.item].discount!)%"
        cell.subcatLabel.text = venues[indexPath.item].catTitle
        cell.nameLabel.text = venues[indexPath.item].title
        
        let url = URL(string: venues[indexPath.item].image!)
        cell.imageView.kf.setImage(with: url, options: [.transition(.fade(0.2))])
        
        cell.discountImageView.isHidden = venues[indexPath.item].discount == 0
        cell.discountLabel.isHidden = cell.discountImageView.isHidden
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        dismiss(animated: true, completion: nil)
        let detailVC = VenueDetailViewController()
        detailVC.id = venues[indexPath.item].id
        detailVC.navigationItem.title = venues[indexPath.item].catTitle
        viewController?.navigationController?.pushViewController(detailVC, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.frame.width-30)/2
        let height = width*0.7
        
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(10, 10, 10, 10)
    }
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let lastItem = collectionView.numberOfItems(inSection: 0)-1
        let noRemainder = venues.count % 10 == 0
        if indexPath.item == lastItem && noRemainder {
            page += 1
            params["page"] = "\(page)"
            searchVenues()
        }
    }
    
}
