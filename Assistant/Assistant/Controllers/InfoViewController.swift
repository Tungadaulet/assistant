//
//  InfoViewController.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 9/27/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import UIKit
import SVProgressHUD
import AlamofireDomain
import SnapKit

class InfoViewController: UIViewController, UIWebViewDelegate {
    
    let cellId = "cellId"
    
    var agreement: String?
    
    let scrollView: UIScrollView = {
        let sv = UIScrollView()
        sv.backgroundColor = .white
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.isDirectionalLockEnabled = true
        sv.alwaysBounceVertical = true
        return sv
    }()
    
    let scrollContentView = UIView()
    
    let webView: UIWebView = {
        let wv = UIWebView()
        wv.scrollView.isScrollEnabled = false
        wv.scrollView.bounces = false
        wv.translatesAutoresizingMaskIntoConstraints = false
        return wv
    }()
    
    let notApplyBtn = InfoViewController.createGradientButton(withTitle: "Не принимаю")
    let applyBtn = InfoViewController.createGradientButton(withTitle: "Принимаю")
    
    static func createGradientButton(withTitle title: String, image: UIImage = UIImage()) -> UIButton {
        let button = UIButton(type: .system)
        button.setTitle(title, for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.layer.cornerRadius = 20
        button.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        button.backgroundColor = .clear
        button.setImage(image, for: .normal)
        button.tintColor = .white
        return button
    }
    
    var webViewHeightConstraint: Constraint?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setAttributedNavigationTitle()
        
        setupViews()
        
        if let string = agreement {
            webView.loadHTMLString(string, baseURL: nil)
            webView.delegate = self
        }
        
        applyBtn.addTarget(self, action: #selector(applyBtnAction), for: .touchUpInside)
        notApplyBtn.addTarget(self, action: #selector(notApplyBtnAction), for: .touchUpInside)
    }
    
    func setAttributedNavigationTitle() {
        let titleLabel = UILabel()
        titleLabel.textColor = .white
        let attributedTitle = NSMutableAttributedString(string: "ASSISTANT ", attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 17)])
        attributedTitle.append(NSAttributedString(string: "APP", attributes: [NSFontAttributeName: UIFont.boldSystemFont(ofSize: 17)]))
        titleLabel.attributedText = attributedTitle
        titleLabel.sizeToFit()
        navigationItem.titleView = titleLabel
    }
    
    func setupViews() {
        view.addSubview(scrollView)
        
        scrollView.snp.makeConstraints { (make) in
            make.edges.equalTo(0)
        }
        
        scrollView.addSubview(scrollContentView)
        
        scrollContentView.addSubview(webView)
        scrollContentView.addSubview(notApplyBtn)
        scrollContentView.addSubview(applyBtn)
        
        webView.snp.makeConstraints { (make) in
            make.top.left.equalTo(0)
            make.width.equalTo(view)
            webViewHeightConstraint = make.height.equalTo(10).constraint
        }
        
        notApplyBtn.snp.makeConstraints { (make) in
            make.top.equalTo(webView.snp.bottom).offset(20)
            make.left.equalTo(16)
            make.right.equalTo(view.snp.centerX).offset(-6)
            make.height.equalTo(40)
        }
        
        applyBtn.snp.makeConstraints { (make) in
            make.top.height.equalTo(notApplyBtn)
            make.left.equalTo(view.snp.centerX).offset(6)
            make.right.equalTo(view).offset(-16)
        }
        
        scrollContentView.snp.makeConstraints { (make) in
            make.top.left.equalTo(0)
            make.width.equalTo(view)
            make.bottom.equalTo(notApplyBtn).offset(10)
        }
        
        scrollView.layoutIfNeeded()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        notApplyBtn.addGradient()
        applyBtn.addGradient()
        
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        let height = webView.scrollView.contentSize.height
        let buttonHeight: CGFloat = 40
        webViewHeightConstraint?.update(offset: height)
        scrollView.contentSize = CGSize(width: scrollView.frame.width, height: height+buttonHeight+30)
    }
    
    func applyBtnAction() {
        if !Connectivity.isConnectedToInternet() {
            showNetworkErrorAlert()
            return
        }
        
        self.addFCMToken {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.initMenuController()
            let menuController = appDelegate.menuController
            self.present(menuController!, animated: true, completion: nil)
        }
    }
    
    func notApplyBtnAction() {
        deleteUser()
    }
    
    func deleteUser() {
        let indicator = SVProgressHUD.self
        indicator.setDefaultMaskType(.gradient)
        indicator.show(withStatus: "Идет загрузка...")
        
        let URL = Const.URL.deleteUser
        
        let token = userDefaults.getAccessToken()
        let headers = ["accessToken": token]
        
        AlamofireDomain.request(URL, method: .post, headers: headers).responseJSON { (response) in
            let statusCode = response.response?.statusCode
            
            indicator.dismiss()
            
            if statusCode == 200  {
                userDefaults.setIsLoggedIn(value: false)
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
}
