//
//  HomeSearchViewController.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 12/22/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import UIKit

class HomeSearchViewController: SearchViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func searchVenues() {
        let searchText = params["search_text"] as! String
        
        SearchedList.fetchVenuesByText(searchText: searchText, page: page) { (venues, statusCode) in
            if self.page == 1 {
                self.venues = venues
            }
            else {
                self.venues += venues as [Venue]
            }
            self.collectionView?.reloadData()
            self.aiv.stopAnimating()
            self.handleError(with: statusCode)
        }
    }
}
