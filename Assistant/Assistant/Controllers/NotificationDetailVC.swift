//
//  NotificationDetailVC.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 10/30/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import UIKit
import SnapKit

class NotificationDetailVC: UIViewController, UIWebViewDelegate {
    
    var height: CGFloat = 0
    
    var notificationId: Int?
    var notification = Notification()
    
    var webViewHeightConstraint: Constraint?
    
    let scrollView: UIScrollView = {
        let sv = UIScrollView()
        sv.backgroundColor = .white
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.isDirectionalLockEnabled = true
        sv.alwaysBounceVertical = true
        return sv
    }()
    
    let scrollContentView = UIView()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        return label
    }()
    
    let dateImageView = UIImageView(image: #imageLiteral(resourceName: "calendar"))
    
    let dateLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 11)
        return label
    }()
    
    let webView: UIWebView = {
        let wv = UIWebView()
        wv.scrollView.isScrollEnabled = false
        wv.scrollView.bounces = false
        wv.translatesAutoresizingMaskIntoConstraints = false
        return wv
    }()
    
    let aiv = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    
    let webViewAiv = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Уведомления"
        
        view.backgroundColor = .white
        
        setupViews()
        
        aiv.startAnimating()
        fetchNotification()
    }
    
    func fetchNotification() {
        Notification.fetchNotificationBy(id: notificationId!) { (notification, statusCode) in
            self.notification = notification
            self.setData()
            self.aiv.stopAnimating()
            self.handleError(with: statusCode)
        }
    }
    
    func setupViews() {
        view.addSubview(scrollView)
        
        scrollView.snp.makeConstraints { (make) in
            make.edges.equalTo(0)
        }
        
        scrollView.addSubview(scrollContentView)
        
        scrollContentView.addSubview(aiv)
        scrollContentView.addSubview(titleLabel)
        scrollContentView.addSubview(dateImageView)
        scrollContentView.addSubview(dateLabel)
        scrollContentView.addSubview(webViewAiv)
        scrollContentView.addSubview(webView)
        
        aiv.snp.makeConstraints { (make) in
            make.top.equalTo(12)
            make.centerX.equalTo(scrollContentView)
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.top.left.equalTo(12)
            make.right.equalTo(-12)
        }
        
        dateImageView.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(8)
            make.left.equalTo(12)
        }
        
        dateLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(dateImageView)
            make.left.equalTo(dateImageView.snp.right).offset(8)
        }
        
        webViewAiv.snp.makeConstraints { (make) in
            make.top.equalTo(dateLabel.snp.bottom).offset(8)
            make.centerX.equalTo(scrollContentView)
        }
        
        webView.snp.makeConstraints { (make) in
            make.top.equalTo(dateLabel.snp.bottom).offset(8)
            make.left.equalTo(10)
            make.right.equalTo(-10)
            webViewHeightConstraint = make.height.equalTo(1).constraint
        }
        
        scrollContentView.snp.makeConstraints { (make) in
            make.top.left.equalTo(0)
            make.width.equalTo(view)
            make.bottom.equalTo(webView)
        }
                
        scrollView.layoutIfNeeded()
    }
    
    func setData() {
        webViewAiv.startAnimating()
        
        titleLabel.text = notification.title
        dateLabel.text = notification.date
        if var content = notification.text {
            let data = "<style>img,iframe{display: inline; height: auto; max-width: 100%;}</style>"
            content = content.replacingOccurrences(of: "//www.", with: "https://")
            
            webView.loadHTMLString(data+content, baseURL: nil)
            webView.delegate = self
        }
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        let height = webView.scrollView.contentSize.height
        webViewHeightConstraint?.update(offset: height)
        scrollView.contentSize = CGSize(width: scrollView.frame.width, height: scrollContentView.frame.height+height)
        
        webViewAiv.stopAnimating()
    }
}
