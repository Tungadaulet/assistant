//
//  LoginViewController.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 9/27/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import UIKit
import AKMaskField
import SnapKit
import FirebaseAuth
import SVProgressHUD
import AlamofireDomain
import SwiftyJSON
import Firebase

class LoginViewController: UIViewController {
    
    let bgImageView = UIImageView(image: #imageLiteral(resourceName: "auth"))
    
    let nameField = LoginViewController.createInputField(withPlaceholder: "Имя")
    let phoneField = LoginViewController.createInputField(withPlaceholder: "", isPhoneField: true, keyboardType: .numberPad)
    let codeField = LoginViewController.createInputField(withPlaceholder: "Код подтверждения", keyboardType: .numberPad)
    
    let sendCodeBtn = StartViewController.createWhiteBorderButton(withTitle: "Отправить код")
    let loginBtn = StartViewController.createWhiteBorderButton(withTitle: "Войти")
    
    static func createInputField(withPlaceholder placeholder: String, isPhoneField: Bool = false, keyboardType: UIKeyboardType = .default) -> AKMaskField {
        let field = AKMaskField()
        let attributedHolder = NSMutableAttributedString(string: placeholder, attributes: [NSForegroundColorAttributeName: UIColor.white])
        field.attributedPlaceholder = attributedHolder
        field.font = UIFont.systemFont(ofSize: 14)
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: 15))
        field.leftView = paddingView
        field.leftViewMode = .always
        field.rightView = paddingView
        field.rightViewMode = .always
        field.layer.borderWidth = 1
        field.layer.borderColor = UIColor.white.cgColor
        field.textColor = .white
        field.backgroundColor = UIColor.white.withAlphaComponent(0.5)
        field.layer.cornerRadius = 20
        field.keyboardType = keyboardType
        if isPhoneField {
            field.setMask("+7({ddd}) {ddd} {dd} {dd}", withMaskTemplate: "+7(***) *** ** **")
        }
        return field
    }
    
    var phoneCenterY: Constraint?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
    }
    
    func setupViews() {
        view.addSubview(bgImageView)
        view.addSubview(nameField)
        view.addSubview(phoneField)
        view.addSubview(sendCodeBtn)
        view.addSubview(codeField)
        view.addSubview(loginBtn)
        
        bgImageView.snp.makeConstraints { (make) in
            make.edges.equalTo(0)
        }
        
        nameField.snp.makeConstraints { (make) in
            make.left.equalTo(30)
            make.right.equalTo(-30)
            make.bottom.equalTo(phoneField.snp.top).offset(-6)
            make.height.equalTo(40)
        }
        
        phoneField.snp.makeConstraints { (make) in
            phoneCenterY = make.centerY.equalTo(view).constraint
            make.left.right.height.equalTo(nameField)
        }
        
        sendCodeBtn.snp.makeConstraints { (make) in
            make.top.equalTo(phoneField.snp.bottom).offset(12)
            make.width.equalTo(nameField).dividedBy(2)
            make.height.equalTo(nameField)
            make.centerX.equalTo(view)
        }
        
        codeField.snp.makeConstraints { (make) in
            make.top.equalTo(view.snp.centerY)
            make.left.right.height.equalTo(nameField)
        }
        
        loginBtn.snp.makeConstraints { (make) in
            make.top.equalTo(codeField.snp.bottom).offset(12)
            make.width.height.centerX.equalTo(sendCodeBtn)
        }
        
        codeField.alpha = 0
        loginBtn.alpha = 0
        sendCodeBtn.alpha = 1
        
        sendCodeBtn.addTarget(self, action: #selector(sendCodeBtnAction), for: .touchUpInside)
        loginBtn.addTarget(self, action: #selector(loginBtnAction), for: .touchUpInside)
    }
    
    func sendCodeBtnAction() {
        if !Connectivity.isConnectedToInternet() {
            showNetworkErrorAlert()
            return
        }
        
        //let name = nameField.text!
        let phoneString = phoneField.text!
        let phone = getNormalPhoneFormat(value: phoneString)

//        if name.isEmpty {
//            showAlert(withMessage: "Поле имя не заполнено")
//            return
//        }
        if phone.characters.count != 12 {
            showAlert(withMessage: "Неверный формат номер телефона")
            return
        }
        
        print("phone: ", phone)
        
        let indicator = SVProgressHUD.self
        indicator.setDefaultMaskType(.gradient)
        indicator.show(withStatus: "Идет загрузка...")
        
        if phone == "+70001110000" {
            testAuthForItunes()
            return
        }
        
        PhoneAuthProvider.provider().verifyPhoneNumber(phone, uiDelegate: nil) { (verificationID, error) in
            if error != nil {
                print("err: ", String(describing: error?.localizedDescription))
                
                indicator.dismiss()
                self.handleError(with: 500)
            }
            else {
                print("authVID: ", String(describing: verificationID))
                userDefaults.setAuthVerificationID(value: verificationID!)
                
                indicator.dismiss(completion: {
                    self.animateCodeFieldAndLoginButton()
                })
            }
        }
    }
    
    // MARK: - Test Auth For Itunes
    func testAuthForItunes() {
        let token = "sd465as4d6a5s4dsad64asd65464654s65132" // test token
        userDefaults.setAccessToken(value: token)

        self.addFCMToken(completion: {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.initMenuController()
            let menuController = appDelegate.menuController
            self.present(menuController!, animated: true, completion: nil)
        })
    }
    
    func animateCodeFieldAndLoginButton() {
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.phoneCenterY?.update(offset: -26)
            self.view.layoutIfNeeded()
            self.codeField.alpha = 1
            self.loginBtn.alpha = 1
            self.sendCodeBtn.alpha = 0
            
        }, completion: nil)
    }
    
    func dismissAnimation() {
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.phoneCenterY?.update(offset: 26)
            self.view.layoutIfNeeded()
            self.codeField.alpha = 0
            self.loginBtn.alpha = 0
            self.sendCodeBtn.alpha = 1

        }, completion: nil)
    }
    
    func loginBtnAction() {
        if !Connectivity.isConnectedToInternet() {
            showNetworkErrorAlert()
            return
        }
        
        let code = codeField.text!
        
        if code.isEmpty {
            showAlert(withMessage: "Код подтверждения не указана")
            return
        }
        
        let verificationID = userDefaults.getAuthVerificationID()
        let credential = PhoneAuthProvider.provider().credential(
            withVerificationID: verificationID,
            verificationCode: code)
        
        let indicator = SVProgressHUD.self
        indicator.setDefaultMaskType(.gradient)
        indicator.show(withStatus: "Идет загрузка...")
        
        Auth.auth().signIn(with: credential) { (user, error) in
            if error != nil {
                print("err: ", String(describing: error?.localizedDescription))
                
                indicator.dismiss()
                self.showAlert(withMessage: "Неверный код подтверждения")
            }
            else {
                user?.getIDTokenForcingRefresh(false, completion: { (token, error) in
                    print("firebase auth token", token!)
                    self.authorization(JWT: token!)
                })
            }
        }
    }
    
    func authorization(JWT: String) {
        let name = nameField.text! == "" ? "User" : nameField.text!
        let phoneString = phoneField.text!
        let phone = getNormalPhoneFormat(value: phoneString)
        
        let URL = Const.URL.login
        
        let params = ["name": name, "phone": phone, "jwt": JWT]
        
        AlamofireDomain.request(URL, method: .post, parameters: params).responseJSON { (response) in
            
            if let value = response.result.value {
                let json = JSON(value)
                print("auth json reponse", json)
                
                let token = json["result"]["accessToken"].stringValue
                userDefaults.setAccessToken(value: token)
                
                SVProgressHUD.self.dismiss()
                
                let statusCode = response.response?.statusCode ?? 500
                
                if statusCode == 201 { // created
                    let agreement = json["result"]["agreement"].stringValue
                    let infoVC = InfoViewController()
                    infoVC.agreement = agreement
                    let infoNC = UINavigationController(rootViewController: infoVC)
                    self.present(infoNC, animated: true, completion: nil)
                }
                else if statusCode == 202 { // accepted
                    self.addFCMToken(completion: {
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.initMenuController()
                        let menuController = appDelegate.menuController
                        self.present(menuController!, animated: true, completion: nil)
                    })
                }
                else {
                    self.handleError(with: statusCode)
                }
            }
        }
    }
    
    func getNormalPhoneFormat(value: String) -> String {
        var phone = value.replacingOccurrences(of: "*", with: "")
        phone = phone.replacingOccurrences(of: " ", with: "")
        phone = phone.replacingOccurrences(of: "(", with: "")
        phone = phone.replacingOccurrences(of: ")", with: "")
        return phone
    }
    
    func showAlert(withMessage message: String) {
        let alert = UIAlertController(title: "Ошибка", message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(okAction)
        
        present(alert, animated: true, completion: nil)
    }
}

extension UIViewController {
    
    func addFCMToken(completion: @escaping () -> ()) {
        let indicator = SVProgressHUD.self
        indicator.setDefaultMaskType(.gradient)
        indicator.show(withStatus: "Идет загрузка...")
        
        let URL = Const.URL.addFCMToken
        
        guard let fcmToken = InstanceID.instanceID().token() else {
            return
        }
        let params = ["fcm_token": fcmToken, "device_type": "1"]
        let token = userDefaults.getAccessToken()
        let headers = ["accessToken": token]
        
        AlamofireDomain.request(URL, method: .post, parameters: params, headers: headers).responseJSON { (response) in
            
            indicator.dismiss()
            
            let statusCode = response.response?.statusCode ?? 500
            
            if statusCode == 200 || statusCode == 201 {
                userDefaults.setIsLoggedIn(value: true)
                completion()
            }
            else {
                self.handleError(with: statusCode)
            }
        }
    }
}



