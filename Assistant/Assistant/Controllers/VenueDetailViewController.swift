//
//  VenueDetailViewController.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 10/19/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import UIKit

class VenueDetailViewController: UITableViewController, ReviewsDelegate {
   
    var id: Int?
    var venueDetails = VenueDetails()
    var reviewsDelegate: ReviewsDelegate?
    
    let sliderCellId = "sliderCellId"
    let infoCellId = "infoCellId"
    let scheduleCellId = "scheduleCellId"
    let filtersCellId = "filtersCellId"
    let menuCellId = "menuCellId"
    var extraInfoCellId = "extraInfoCellId"
    let additionalInfoCellId = "additionalInfoCellId"
    let reviewsCellId = "reviewsCellId"
    let linksCellId = "linksCellId"
    let mapCellId = "mapCellId"
    
    var isScheduleVisible = false
    
    let aiv = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        tableView.separatorStyle = .none        
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableViewAutomaticDimension
        
        tableView.register(VenueSliderCell.self, forCellReuseIdentifier: sliderCellId)
        tableView.register(VenueInfoCell.self, forCellReuseIdentifier: infoCellId)
        tableView.register(VenueScheduleCell.self, forCellReuseIdentifier: scheduleCellId)
        tableView.register(VenueFiltersCell.self, forCellReuseIdentifier: filtersCellId)
        tableView.register(VenueMenuCell.self, forCellReuseIdentifier: menuCellId)
        tableView.register(VenueExtraInfoCell.self, forCellReuseIdentifier: extraInfoCellId)
        tableView.register(VenueAdditionalInfoCell.self, forCellReuseIdentifier: additionalInfoCellId)
        tableView.register(VenueReviewsCell.self, forCellReuseIdentifier: reviewsCellId)
        tableView.register(VenueLinksCell.self, forCellReuseIdentifier: linksCellId)
        tableView.register(VenueMapCell.self, forCellReuseIdentifier: mapCellId)
        tableView.backgroundView = aiv
        
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(fetchVenueDetails), for: .valueChanged)
        tableView.addSubview(refreshControl!)
        
        aiv.startAnimating()
        fetchVenueDetails()        
    }
    
    func fetchVenueDetails() {
        guard let venueId = id else { return }
        VenueDetails.fetchVenueDetailBy(id: venueId) { (venueDetails, statusCode) in
            self.venueDetails = venueDetails
            self.tableView.reloadData()
            self.refreshControl?.endRefreshing()
            self.aiv.stopAnimating()
            self.handleError(with: statusCode)
        }
    }
    
    func setReviewsCount(_ count: Int?) {
        self.venueDetails.reviewsCount = count
        tableView.reloadData()
        self.reviewsDelegate?.setReviewsCount(count)
    }
    
    func getRowsCount() -> Int {
        var count = 0
        
        if venueDetails.id == nil {
            return count
        }
        
        count = 5
        
        if isScheduleVisible {
            count += 1
        }
        if venueDetails.filters.count > 0 {
            count += 1
        }
        if venueDetails.menu.id != 0 && venueDetails.menu.id != nil {
            count += 1
        }
        if venueDetails.extraInfo.count > 0 {
            count += 1
        }
        if venueDetails.links.count > 0 {
            count += 1
        }
        
        return count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getRowsCount()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let rowsCount = tableView.numberOfRows(inSection: 0)
        
        var scheduleRow = rowsCount
        var filtersRow = rowsCount
        var menuRow = rowsCount
        var extraInfoRow = rowsCount
        var infoRow = rowsCount
        var reviewsRow = rowsCount
        var linksRow = rowsCount
        
        if isScheduleVisible {
            scheduleRow = 2
        }
        if venueDetails.filters.count > 0 {
            filtersRow = 2
            filtersRow += scheduleRow != rowsCount ? 1 : 0
        }
        if venueDetails.menu.id != 0 && venueDetails.menu.id != nil {
            menuRow = 2
            menuRow += scheduleRow != rowsCount ? 1 : 0
            menuRow += filtersRow != rowsCount ? 1 : 0
        }
        if venueDetails.extraInfo.count > 0 {
            extraInfoRow = 2
            extraInfoRow += (scheduleRow != rowsCount) ? 1 : 0
            extraInfoRow += (filtersRow != rowsCount) ? 1 : 0
            extraInfoRow += (menuRow != rowsCount) ? 1 : 0
        }
        infoRow = 6;
        infoRow -= (scheduleRow == rowsCount) ? 1 : 0
        infoRow -= (filtersRow == rowsCount) ? 1 : 0
        infoRow -= (menuRow == rowsCount) ? 1 : 0
        infoRow -= (extraInfoRow == rowsCount) ? 1 : 0
        reviewsRow = 7;
        reviewsRow -= 6-infoRow
        if venueDetails.links.count > 0 {
            linksRow = reviewsRow+1
        }
        
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: sliderCellId, for: indexPath) as! VenueSliderCell
            
            cell.images = venueDetails.images
            
            if let discount = venueDetails.discount {
                cell.discountLabel.text = "-\(discount)%"
                cell.discountImageView.isHidden = discount == 0
                cell.discountLabel.isHidden = cell.discountImageView.isHidden
            }
            
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: infoCellId, for: indexPath) as! VenueInfoCell
            
            cell.nameLabel.text = venueDetails.title
            
            if let rating = venueDetails.rating, let reviewCount = venueDetails.reviewsCount, let workTime = venueDetails.workTime {
                let ratingAttString = NSMutableAttributedString(string: "Рейтинг: ", attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 14)])
                ratingAttString.append(NSAttributedString(string: "\(rating)", attributes: [NSFontAttributeName: UIFont.boldSystemFont(ofSize: 14)]))
                cell.ratingLabel.attributedText = ratingAttString
                
                cell.ratingImageView.image = UIImage(named: "rating-\(rating)")
                
                let reviewAttString = NSMutableAttributedString(string: "Отзывы: ", attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 14)])
                reviewAttString.append(NSAttributedString(string: "\(reviewCount)", attributes: [NSFontAttributeName: UIFont.boldSystemFont(ofSize: 14)]))
                cell.reviewLabel.attributedText = reviewAttString
                
                let timeAttString = NSMutableAttributedString(string: "Сегодня: ", attributes: [NSFontAttributeName: UIFont.boldSystemFont(ofSize: 13)])
                timeAttString.append(NSAttributedString(string: workTime
                    , attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 13)]))
                cell.workTimeLabel.attributedText = timeAttString
            }
            
            addScheduleTapGestureToView(cell.timeView)
            addScheduleTapGestureToView(cell.workTimeLabel)

            return cell
        case scheduleRow:
            let cell = tableView.dequeueReusableCell(withIdentifier: scheduleCellId, for: indexPath) as! VenueScheduleCell
            
            cell.weekTimes = venueDetails.weekTimes
            
            return cell
        case filtersRow:
            let cell = tableView.dequeueReusableCell(withIdentifier: filtersCellId, for: indexPath) as! VenueFiltersCell
            
            cell.filters = venueDetails.filters
        
            return cell
        case menuRow:
            let cell = tableView.dequeueReusableCell(withIdentifier: menuCellId, for: indexPath) as! VenueMenuCell
            
            if let buttonTitle = venueDetails.menu.title {
                cell.menuButton.setTitle(" " + buttonTitle, for: .normal)
                cell.menuButton.addTarget(self, action: #selector(showVenueMenu), for: .touchUpInside)
            }
            
            return cell
        case extraInfoRow:
            let cell = tableView.dequeueReusableCell(withIdentifier: extraInfoCellId, for: indexPath) as! VenueExtraInfoCell
            
            cell.venueExtraInfo = venueDetails.extraInfo
            
            return cell
        case infoRow:
            let cell = tableView.dequeueReusableCell(withIdentifier: additionalInfoCellId, for: indexPath) as! VenueAdditionalInfoCell
        
            cell.phones = venueDetails.phones
            cell.venueInfo = venueDetails.info            
            
            return cell
        case reviewsRow:
            let cell = tableView.dequeueReusableCell(withIdentifier: reviewsCellId, for: indexPath) as! VenueReviewsCell
            
            if let reviewsCount = venueDetails.reviewsCount {
                cell.reviewsButton.setTitle(" Отзывы (\(reviewsCount))", for: .normal)
                cell.reviewsButton.addTarget(self, action: #selector(showReviews), for: .touchUpInside)
            }
            
            return cell
        case linksRow:
            let cell = tableView.dequeueReusableCell(withIdentifier: linksCellId, for: indexPath) as! VenueLinksCell
            
            cell.links = venueDetails.links
            
            return cell
        case rowsCount-1:
            let cell = tableView.dequeueReusableCell(withIdentifier: mapCellId, for: indexPath) as! VenueMapCell
            
            cell.latitude = venueDetails.latitude
            cell.longitude = venueDetails.longitude
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(showInMap))
            cell.gradientView.addGestureRecognizer(tap)
            
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func addScheduleTapGestureToView(_ view: UIView) {
        let tap = UITapGestureRecognizer(target: self, action: #selector(showSchedule))
        view.addGestureRecognizer(tap)
    }
    
    func showSchedule() {
        isScheduleVisible = !isScheduleVisible
        
        tableView.beginUpdates()
        
        let indexPath = IndexPath(row: 2, section: 0)
        
        if isScheduleVisible {
            tableView.insertRows(at: [indexPath], with: .automatic)
        }
        else {
            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
        
        tableView.endUpdates()
    }
    
    func showInMap() {
        let mapVC = AddressInMapVC()
        mapVC.address = venueDetails.info[0].value
        mapVC.latitude = venueDetails.latitude
        mapVC.longitude = venueDetails.longitude
        mapVC.navigationItem.title = venueDetails.title
        
        navigationController?.pushViewController(mapVC, animated: true)
    }
    
    func showVenueMenu() {
        let venueMenuVC = VenueMenuViewController()
        guard let menu = venueDetails.menu.title else {
            return
        }
        guard let name = venueDetails.title else {
            return
        }
        venueMenuVC.navigationItem.title = menu + " " + "«\(name)»"
        venueMenuVC.menu = venueDetails.menu
        venueMenuVC.serviceCharge = venueDetails.serviceCharge
        venueMenuVC.discount = venueDetails.discount
        navigationController?.pushViewController(venueMenuVC, animated: true)
    }
    
    func showReviews() {
        let reviewsVC = ReviewsViewController()
        reviewsVC.navigationItem.title = venueDetails.title
        reviewsVC.venueId = id
        reviewsVC.delegate = self
        navigationController?.pushViewController(reviewsVC, animated: true)
    }
}
