//
//  NotificationsViewController.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 10/30/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import UIKit

class NotificationsViewController: UITableViewController {
    
    var notifications = [Notification]()
    var page = 1
    
    let cellId = "cellId"
    
    let aiv = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addLeftMenuNavBarItem()
        navigationItem.title = "Уведомления"
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        tableView.separatorInset = .zero
        tableView.separatorColor = .black
        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.backgroundView = aiv
        tableView.tableFooterView = UIView()
        
        tableView.register(NotificationCell.self, forCellReuseIdentifier: cellId)
        
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(refreshList), for: .valueChanged)
        tableView.addSubview(refreshControl!)
        
        aiv.startAnimating()
        fetchNotifications()
    }
    
    func refreshList() {
        page = 1
        fetchNotifications()
    }
    
    func fetchNotifications() {
        Notification.fetchNotifications(page: page) { (notifications) in
            if self.page == 1 {
                self.notifications = notifications
            }
            else {
                self.notifications += notifications
            }
            self.tableView.reloadData()
            self.aiv.stopAnimating()
            self.refreshControl?.endRefreshing()
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notifications.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! NotificationCell
        
        cell.titleLabel.text = notifications[indexPath.row].title
        cell.dateLabel.text = notifications[indexPath.row].date
        cell.textView.text = notifications[indexPath.row].description
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let notificationDetailVC = NotificationDetailVC()
        notificationDetailVC.notificationId = notifications[indexPath.row].id
        navigationController?.pushViewController(notificationDetailVC, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastRow = tableView.numberOfRows(inSection: 0)-1
        let noRemainder = notifications.count % 10 == 0
        
        if indexPath.row == lastRow && noRemainder {
            page += 1
            fetchNotifications()
        }
    }
}
