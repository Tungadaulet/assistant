//
//  FilterInfoViewController.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 10/30/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import UIKit

class FilterInfoViewController: UIViewController {
  
    let tipLabel: UILabel = {
        let label = UILabel()
        label.textColor = Const.Colors.navBar
        label.font = UIFont.systemFont(ofSize: 14)
        label.text = "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    let valueLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 15)
        label.textColor = Const.Colors.navBar
        label.layer.borderColor = UIColor.lightGray.cgColor
        label.layer.borderWidth = 2
        label.layer.cornerRadius = 4
        label.textAlignment = .center
        return label
    }()
    
    let plusButton = FilterInfoViewController.createValueChangeButton(with: "+", tag: 1)
    let minusButton = FilterInfoViewController.createValueChangeButton(with: "-")
    
    static func createValueChangeButton(with title: String, tag: Int = 0) -> UIButton {
        let button = UIButton(type: .system)
        button.setTitleColor(Const.Colors.navBar, for: .normal)
        button.setTitle(title, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 30)
        button.tag = tag
        return button
    }
    
    let minValueLabel: UILabel = {
        let label = UILabel()
        label.textColor = Const.Colors.navBar
        label.font = UIFont.systemFont(ofSize: 14)
        return label
    }()
    
    let maxValueLabel: UILabel = {
        let label = UILabel()
        label.textColor = Const.Colors.navBar
        label.font = UIFont.systemFont(ofSize: 14)
        return label
    }()
    
    let slider: UISlider = {
        let slider = UISlider()
        slider.minimumTrackTintColor = Const.Colors.navBar
        slider.maximumTrackTintColor = UIColor.lightGray
        return slider
    }()
    
    let okButton = InfoViewController.createGradientButton(withTitle: "OK")
    
    var filteredList = FilteredList()
    var infoIndex = 0
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.menuController?.openDrawerGestureModeMask = .init(rawValue: 0)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.menuController?.openDrawerGestureModeMask = .panningCenterView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.hidesBackButton = true
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(backAction))
        
        view.backgroundColor = .white
        
        setupViews()
        
        guard let minValue = filteredList.info[infoIndex].minValue else {
            return
        }
        guard let maxValue = filteredList.info[infoIndex].maxValue else {
            return
        }
        guard let endString = filteredList.info[infoIndex].endString else {
            return
        }
        guard var value = filteredList.info[infoIndex].value else {
            return
        }
        
        if value == 0 {
            value = minValue
            filteredList.info[infoIndex].value = minValue
        }
        slider.minimumValue = Float(minValue)
        slider.maximumValue = Float(maxValue)
        slider.value = Float(value)
        
        minValueLabel.text = "\(minValue) " + endString
        maxValueLabel.text = "\(maxValue) " + endString
        valueLabel.text = "\(value) " + endString
        tipLabel.text = filteredList.info[infoIndex].description
    }
    
    func backAction() {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func setupViews() {
        view.addSubview(tipLabel)
        view.addSubview(valueLabel)
        view.addSubview(plusButton)
        view.addSubview(minusButton)
        view.addSubview(minValueLabel)
        view.addSubview(maxValueLabel)
        view.addSubview(slider)
        view.addSubview(okButton)
        
        tipLabel.snp.makeConstraints { (make) in
            make.top.equalTo(76)
            make.left.equalTo(12)
            make.right.equalTo(-12)
        }
        
        valueLabel.snp.makeConstraints { (make) in
            make.top.equalTo(tipLabel.snp.bottom).offset(12)
            make.centerX.equalTo(view)
            make.width.equalTo(view).dividedBy(3)
            make.height.equalTo(40)
        }
        
        plusButton.snp.makeConstraints { (make) in
            make.left.equalTo(valueLabel.snp.right).offset(12)
            make.centerY.equalTo(valueLabel)
        }
        
        minusButton.snp.makeConstraints { (make) in
            make.right.equalTo(valueLabel.snp.left).offset(-12)
            make.centerY.equalTo(valueLabel)
        }
        
        minValueLabel.snp.makeConstraints { (make) in
            make.top.equalTo(valueLabel.snp.bottom).offset(12)
            make.left.equalTo(20)
        }
        
        maxValueLabel.snp.makeConstraints { (make) in
            make.top.equalTo(minValueLabel)
            make.right.equalTo(-20)
        }
        
        slider.snp.makeConstraints { (make) in
            make.top.equalTo(minValueLabel.snp.bottom)
            make.left.equalTo(minValueLabel)
            make.right.equalTo(maxValueLabel)
        }
        
        okButton.snp.makeConstraints { (make) in
            make.top.equalTo(slider.snp.bottom).offset(30)
            make.width.equalTo(view).dividedBy(3)
            make.centerX.equalTo(view)
            make.height.equalTo(40)
        }
        
        slider.addTarget(self, action: #selector(sliderChanged), for: .valueChanged)
        okButton.addTarget(self, action: #selector(okBtnAction), for: .touchUpInside)
        
        minusButton.addTarget(self, action: #selector(valueChangerBtnClicked(sender:)), for: .touchUpInside)
        plusButton.addTarget(self, action: #selector(valueChangerBtnClicked(sender:)), for: .touchUpInside)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        okButton.addGradient()
    }
    
    func valueChangerBtnClicked(sender: UIButton) {
        guard let minValue = filteredList.info[infoIndex].minValue else {
            return
        }
        guard let maxValue = filteredList.info[infoIndex].maxValue else {
            return
        }
        guard let endString = filteredList.info[infoIndex].endString else {
            return
        }
        guard var value = filteredList.info[infoIndex].value else {
            return
        }
        
        if sender.tag == 0 {
            if value == minValue {
                return
            }
            value -= 1
        }
        else {
            if value == maxValue {
                return
            }
            value += 1
        }
        
        slider.value = Float(value)
        filteredList.info[infoIndex].value = value
        valueLabel.text = "\(value) " + endString
        filteredList.info[infoIndex].valueString = value == minValue ? nil : valueLabel.text
    }
    
    func sliderChanged(sender: UISlider) {
        filteredList.info[infoIndex].value = Int(sender.value)
        
        guard let minValue = filteredList.info[infoIndex].minValue else {
            return
        }
        guard let endString = filteredList.info[infoIndex].endString else {
            return
        }
        guard let value = filteredList.info[infoIndex].value else {
            return
        }
        
        valueLabel.text = "\(value) " + endString
        filteredList.info[infoIndex].valueString = value == minValue ? nil : valueLabel.text
    }
    
    func okBtnAction() {
        let filteredVenuesVC = navigationController?.viewControllers[0] as! FilteredVenuesViewController
        filteredVenuesVC.filteredList = filteredList
        _ = navigationController?.popToViewController(filteredVenuesVC, animated: true)
    }
    
    
}

