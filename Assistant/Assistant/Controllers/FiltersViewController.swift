//
//  FiltersViewController.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 10/30/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import UIKit

class FiltersViewController: RegionsViewController {
    
    var filterIndex = 0
    
    override func viewDidLoad() {
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "OK", style: .plain, target: self, action: #selector(okBtnAction))
        
        tableView.separatorInset = .zero
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellId)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredList.filters[filterIndex].values.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        
        cell.textLabel?.textColor = Const.Colors.navBar
        cell.textLabel?.font = UIFont.systemFont(ofSize: 14)
        
        cell.textLabel?.text = filteredList.filters[filterIndex].values[indexPath.row].value
        
        let checked = filteredList.filters[filterIndex].values[indexPath.row].checked == 1
        cell.accessoryView = UIImageView(image: checked ? #imageLiteral(resourceName: "check-mark") : UIImage())
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let checked = filteredList.filters[filterIndex].values[indexPath.row].checked == 1
        filteredList.filters[filterIndex].values[indexPath.row].checked = checked ? 0 : 1
        
        var checkedValues = ""
        
        for filterValue in filteredList.filters[filterIndex].values {
            if filterValue.checked == 1 {
                checkedValues += "\(filterValue.value!),"
            }
        }
        if checkedValues != "" {
            checkedValues = checkedValues.substring(to: checkedValues.index(checkedValues.endIndex, offsetBy: -1))
        }
        filteredList.filters[filterIndex].checkedValues = checkedValues
        
        tableView.reloadData()
    }
    
    
}
