//
//  VenueMenu.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 10/20/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import Foundation

class VenueMenuPrice {
    
    var id: Int?
    var title: String?
    var values = [VenueMenuPriceValue]()
}
