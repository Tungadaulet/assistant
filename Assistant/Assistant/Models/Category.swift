//
//  Category.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 10/14/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import AlamofireDomain
import SwiftyJSON

class Category: NSObject, NSCoding {
    
    var id: Int?
    var title: String?
    var subcats = [Subcategory]()
    
    override init() {}
    
    init(id: Int, title: String, subcats: [Subcategory]) {
        self.id = id
        self.title = title
        self.subcats = subcats
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(title, forKey: "title")
        aCoder.encode(subcats, forKey: "subcats")
    }
    
    required init?(coder aDecoder: NSCoder) {
        id = aDecoder.decodeObject(forKey: "id") as? Int
        title = aDecoder.decodeObject(forKey: "title") as? String
        subcats = aDecoder.decodeObject(forKey: "subcats") as! [Subcategory]
    }
    
    static func fetchCategories(completionHandler: @escaping ([Category], Int) -> ()) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        let URL = Const.URL.categories
        
        let token = userDefaults.getAccessToken()
        let headers = ["accessToken": token]
        
        var categories = [Category]()
        
        AlamofireDomain.request(URL, method: .get, headers: headers).validate().responseJSON { (response) in
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            let statusCode = response.response?.statusCode ?? 500
            
            if let value = response.result.value {
                let json = JSON(value)

                for result in json["result"].arrayValue {
                    let id = result["id"].intValue
                    let title = result["title"].stringValue
                    
                    var subcats = [Subcategory]()

                    for subcat in result["sub_cats"].arrayValue {
                        let id = subcat["id"].intValue
                        let title = subcat["title"].stringValue
                        let venuesCount = subcat["venues_count"].stringValue
                        
                        let subcat = Subcategory(id: id, title: title, count: venuesCount)
                        subcats.append(subcat)
                    }
                    
                    let category = Category(id: id, title: title, subcats: subcats)
                    categories.append(category)
                }

                let notificationCat = Category(id: 10, title: "Уведомления", subcats: [Subcategory]())
                categories.append(notificationCat)
                
                completionHandler(categories, statusCode)
            }
        }
    }
}

