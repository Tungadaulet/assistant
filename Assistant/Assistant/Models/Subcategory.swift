//
//  Subcategory.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 10/14/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import Foundation

class Subcategory: NSObject, NSCoding {
    
    var id: Int?
    var title: String?
    var venuesCount: String?
    
    override init() {}
    
    init(id: Int, title: String, count: String) {
        self.id = id
        self.title = title
        self.venuesCount = count
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(title, forKey: "title")
        aCoder.encode(venuesCount, forKey: "venuesCount")
    }
    
    required init?(coder aDecoder: NSCoder) {
        id = aDecoder.decodeObject(forKey: "id") as? Int
        title = aDecoder.decodeObject(forKey: "title") as? String
        venuesCount = aDecoder.decodeObject(forKey: "venuesCount") as? String
    }
}
