//
//  Venue.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 10/19/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import Foundation

class Venue {
    
    var id: Int?
    var title: String?
    var image: String?
    var rating: String?
    var catTitle: String?
    var reviewsCount: String?
    var discount: Int?
    
    
}
