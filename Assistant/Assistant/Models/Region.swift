//
//  Region.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 10/29/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import Foundation

class Region {
    
    var id: Int?
    var title: String?
    var checked: Int?    
}
