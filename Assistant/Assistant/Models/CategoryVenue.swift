//
//  CategoryVenues.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 10/19/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import AlamofireDomain
import SwiftyJSON

class CategoryVenue: Venue {
    
    static func fetchVenuesByCat(id: Int, page: Int, searchText: String = "", completionHandler: @escaping ([CategoryVenue], Int, Int) -> ()) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true

        let URL = Const.URL.venuesByCatId
        
        let token = userDefaults.getAccessToken()
        let headers = ["accessToken": token]
        let params = ["menu_id": "\(id)", "page": "\(page)", "perPage": "10", "search_text": searchText]
        
        var catVenues = [CategoryVenue]()
        
        AlamofireDomain.request(URL, method: .get, parameters: params, headers: headers).validate().responseJSON { (response) in
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            let statusCode = response.response?.statusCode ?? 500
            
            if let value = response.result.value {
                let json = JSON(value)
                
                for ven in json["result"]["venues"].arrayValue {
                    let venue = CategoryVenue()
                    venue.id = ven["id"].intValue
                    venue.title = ven["title"].stringValue
                    venue.image = Const.URL.domain + ven["image"].stringValue
                    venue.rating = ven["rating"].stringValue
                    venue.catTitle = ven["category_title"].stringValue
                    venue.reviewsCount = ven["reviews_count"].stringValue
                    venue.discount = ven["discount"].intValue
                    
                    catVenues.append(venue)
                }
                
                let count = json["result"]["details"]["count"].intValue
                
                completionHandler(catVenues, count, statusCode)
            }
            
        }
    }
}
