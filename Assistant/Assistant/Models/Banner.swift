//
//  Banner.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 11/6/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import Foundation

class Banner {
    
    var id: Int?
    var title: String?
    var image: String?
    var link: String?
    
}
