//
//  Review.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 10/25/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import AlamofireDomain
import SwiftyJSON

class Review {
    
    var text: String?
    var name: String?
    var date: String?
    
    static func fetchReviewsOfVenue(id: Int, page: Int, completionHandler: @escaping ([Review], Int?, Int) -> ()) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true

        let URL = Const.URL.reviews
        
        let token = userDefaults.getAccessToken()
        let headers = ["accessToken": token]
        let params = ["venue_id": "\(id)", "page": "\(page)", "perPage": "10"]
        
        var reviews = [Review]()
        
        AlamofireDomain.request(URL, method: .get, parameters: params, headers: headers).validate().responseJSON { (response) in
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            let statusCode = response.response?.statusCode ?? 500
            
            if let value = response.result.value {
                let json = JSON(value)
                
                print("reviews json", json)
                
                for result in json["result"]["reviews"].arrayValue {
                    let review = Review()
                    review.text = result["text"].stringValue
                    review.name = result["name"].stringValue
                    review.date = result["date"].stringValue
                    reviews.append(review)
                }
                
                let count = json["result"]["details"]["count"].intValue
                
                completionHandler(reviews, count, statusCode)
            }
        }
    }
}
