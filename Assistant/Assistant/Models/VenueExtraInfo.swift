//
//  VenueExtraInfo.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 11/12/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import Foundation

class VenueExtraInfo {
    
    var title: String?
    var text: String?
    var isMarked = false
}
