//
//  Filter.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 10/29/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import Foundation

class Filter {
    
    var id: Int?
    var title: String?
    var values = [FilterValue]()
    var isLong = false
    var checkedValues: String? = ""
}
