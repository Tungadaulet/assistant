//
//  WeekTimes.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 12/20/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import Foundation

class WeekTimes {
    
    var weekDay: Int?
    var openTime: String?
    var closeTime: String?
    var otherTime: String?
    var isToday: Bool = false
}
