//
//  VenueInfo.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 10/19/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import AlamofireDomain
import SwiftyJSON

class VenueDetails {
    
    var id: Int?
    var title: String?
    var rating: Int?
    var latitude: Double?
    var longitude: Double?
    var images = [String]()
    var links = [String]()
    var catId: Int?
    var catTitle: String?
    var discount: Int?
    var serviceCharge: Int?
    var reviewsCount: Int?
    var workTime: String?
    var phones = [String]()
    var filters = [VenueFilter]()
    var info = [VenueInfo]()
    var menu = VenueMenu()
    var extraInfo = [VenueExtraInfo]()
    var weekTimes = [WeekTimes]()
    
    static func fetchVenueDetailBy(id: Int, completionHandler: @escaping (VenueDetails, Int) -> ()) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true

        let URL = Const.URL.venue
        
        let token = userDefaults.getAccessToken()
        let headers = ["accessToken": token]
        let params = ["venue_id": "\(id)"]
        
        let venueDetails = VenueDetails()
        
        AlamofireDomain.request(URL, method: .get, parameters: params, headers: headers).validate().responseJSON { (response) in
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            let statusCode = response.response?.statusCode ?? 500
            
            if statusCode == 200 {
                if let value = response.result.value {
                    let json = JSON(value)
                    let result = json["result"]
                    
                    venueDetails.id = result["id"].intValue
                    venueDetails.title = result["title"].stringValue
                    
                    let addressInfo = VenueInfo()
                    addressInfo.title = "Адрес"
                    addressInfo.value = result["address"].stringValue
                    venueDetails.info.append(addressInfo)
                    let phoneInfo = VenueInfo()
                    phoneInfo.title = "Телефон"
                    var phonesValue = ""
                    let phones = result["phone"].arrayValue
                    for (index, phone) in phones.enumerated() {
                        if (index != phones.count-1) {
                            phonesValue += phone.stringValue + "\n"
                        }
                        else {
                            phonesValue += phone.stringValue
                        }
                        venueDetails.phones.append(phone.stringValue)
                    }
                    phoneInfo.value = phonesValue
                    venueDetails.info.append(phoneInfo)

                    venueDetails.rating = result["rating"].intValue
                    venueDetails.latitude = result["latitude"].doubleValue
                    venueDetails.longitude = result["longitude"].doubleValue
                    venueDetails.images.append(Const.URL.domain + result["image"].stringValue)
                    
                    for image in result["medias"].arrayValue {
                        venueDetails.images.append(Const.URL.domain + image.stringValue)
                    }
                    for link in result["discount_links"].arrayValue {
                        venueDetails.links.append(link.stringValue)
                    }
                    venueDetails.catId = result["category_id"].intValue
                    venueDetails.catTitle = result["category_title"].stringValue
                    venueDetails.discount = result["discount"].intValue
                    venueDetails.serviceCharge = result["service_charge"].intValue
                    venueDetails.reviewsCount = result["reviews_count"].intValue
                    venueDetails.workTime = result["work_time_today"].stringValue
                    
                    for filter in result["filters"].arrayValue {
                        let venueFilter = VenueFilter()
                        venueFilter.id = filter["filter_id"].intValue
                        venueFilter.title = filter["filter_title"].stringValue
                        venueFilter.isMarked = filter["filter_is_marked"].boolValue
                        
                        var valueStr = ""
                        
                        for value in filter["values"].arrayValue {
                            venueFilter.values.append(value.stringValue)
                            valueStr += value.stringValue + ", "
                        }
                        if valueStr != "" {
                            valueStr = valueStr.substring(to: valueStr.index(valueStr.endIndex, offsetBy: -2))
                        }                        
                        venueFilter.valueString = valueStr
                        
                        venueDetails.filters.append(venueFilter)
                    }
                    
                    for info in result["information"].arrayValue {
                        let venueInfo = VenueInfo()
                        venueInfo.id = info["information_id"].intValue
                        venueInfo.title = info["information_title"].stringValue
                        venueInfo.value = info["value"].stringValue + " " + info["end_string"].stringValue
                        venueInfo.isMarked = info["is_marked"].boolValue
                        venueDetails.info.append(venueInfo)
                    }
                    
                    venueDetails.menu.id = result["menue_price"]["menu_price_id"].intValue
                    venueDetails.menu.title = result["menue_price"]["menu_price_title"].stringValue
                    
                    for price in result["menue_price"]["price"].arrayValue {
                        let menuPrice = VenueMenuPrice()
                        menuPrice.id = price["id"].intValue
                        menuPrice.title = price["title"].stringValue
                        
                        for value in price["values"].arrayValue {
                            let priceValue = VenueMenuPriceValue()
                            priceValue.title = value["price_value_title"].stringValue
                            priceValue.price = value["price_value"].stringValue
                            menuPrice.values.append(priceValue)
                        }
                        
                        venueDetails.menu.prices.append(menuPrice)
                    }
                    
                    for info in result["extra_information"].arrayValue {
                        let extraInfo = VenueExtraInfo()
                        extraInfo.title = info["title"].stringValue
                        extraInfo.text = info["text"].stringValue
                        extraInfo.isMarked = info["is_marked"].boolValue
                        venueDetails.extraInfo.append(extraInfo)
                    }
                    
                    for time in result["week_times"].arrayValue {
                        let weekTime = WeekTimes()
                        weekTime.weekDay = time["week_day"].intValue
                        weekTime.openTime = time["open_time"].stringValue
                        weekTime.closeTime = time["close_time"].stringValue
                        weekTime.otherTime = time["other_time"].stringValue
                        weekTime.isToday = time["is_today"].boolValue
                        venueDetails.weekTimes.append(weekTime)
                    }
                    
                    completionHandler(venueDetails, statusCode)
                }
            }
            else {
                completionHandler(venueDetails, statusCode)
            }
        }
    }
    
}
