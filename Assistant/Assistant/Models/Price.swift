//
//  Price.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 12/5/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import Foundation

class Price {
    
    var id: Int?
    var title: String?
    var subprices = [Subprice]()
}
