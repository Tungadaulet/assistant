//
//  WorkTime.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 10/29/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import Foundation

class WorkTime {
    
    var id: Int?
    var value: String?
    var checked: Int?
}
