//
//  FilterInfo.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 10/29/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import Foundation

class Info {
    
    var id: Int?
    var title: String?
    var minValue: Int?
    var maxValue: Int?
    var endString: String?
    var value: Int?
    var valueString: String?
    var description: String?
}
