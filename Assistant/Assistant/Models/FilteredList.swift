//
//  FilteredList.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 10/29/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import AlamofireDomain
import SwiftyJSON

class FilteredList {
    
    var regions = [Region]()
    var checkedRegion: String?
    var workTimes = [WorkTime]()
    var checkedWorkTime: String?
    var sortings = [Sorting]()
    var filters = [Filter]()
    var info = [Info]()
    var venues = [Venue]()
    var prices = [Price]()
    var checkedPrices = ""
    
    static func fetchFilteredList(params: Parameters, completionHandler: @escaping (FilteredList, Int) -> ()) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true

        let URL = Const.URL.filteredVenues
        
        let token = userDefaults.getAccessToken()
        let headers = ["accessToken": token]
        
        print("filter params", params)
        
        let filteredList = FilteredList()
        
        AlamofireDomain.request(URL, method: .get, parameters: params, headers: headers).responseJSON { (response) in
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            let statusCode = response.response?.statusCode ?? 500
            
            if let value = response.result.value {
                let json = JSON(value)
                let result = json["result"]
                
                print("filtered list response ", json)
                
                for reg in result["regions"].arrayValue {
                    let region = Region()
                    region.id = reg["id"].intValue
                    region.title = reg["title"].stringValue
                    region.checked = reg["checked"].intValue
                    if region.checked == 1 {
                        filteredList.checkedRegion = region.title
                    }
                    filteredList.regions.append(region)
                }
                
                for time in result["work_times"].arrayValue {
                    let workTime = WorkTime()
                    workTime.id = time["id"].intValue
                    workTime.value = time["value"].stringValue
                    workTime.checked = time["checked"].intValue
                    if workTime.checked == 1 {
                        filteredList.checkedWorkTime = workTime.value
                    }
                    filteredList.workTimes.append(workTime)
                }
                
                for dict in result["sorting"].arrayValue {
                    let sorting = Sorting()
                    sorting.id = dict["id"].intValue
                    sorting.value = dict["value"].stringValue
                    sorting.checked = dict["checked"].intValue
                    filteredList.sortings.append(sorting)
                }
                
                var isLong = true
                
                for dict in result["filters"].arrayValue {
                    let filter = Filter()
                    filter.id = dict["filter_id"].intValue
                    filter.title = dict["filter_title"].stringValue
                    if !dict["is_long"].boolValue {
                        isLong = false
                    }
                    
                    filter.isLong = isLong
                    
                    var checkedValues = ""
                    
                    for value in dict["values"].arrayValue {
                        let filterValue = FilterValue()
                        filterValue.id = value["filter_value_id"].intValue
                        filterValue.value = value["value"].stringValue
                        filterValue.checked = value["checked"].intValue
                        filter.values.append(filterValue)
                        
                        if filterValue.checked == 1 {
                            checkedValues += "\(filterValue.value!),"
                        }
                    }
                    if checkedValues != "" {
                        checkedValues = checkedValues.substring(to: checkedValues.index(checkedValues.endIndex, offsetBy: -1))
                    }
                    filter.checkedValues = checkedValues
                    
                    filteredList.filters.append(filter)
                }
                
                for dict in result["information"].arrayValue {
                    let info = Info()
                    info.id = dict["information_id"].intValue
                    info.title = dict["information_title"].stringValue
                    info.minValue = dict["min_value"].intValue
                    info.maxValue = dict["max_value"].intValue
                    info.endString = dict["end_string"].stringValue
                    info.value = dict["value"].intValue
                    info.description = dict["description"].stringValue
                    
                    if info.value != 0 {
                        info.valueString = "\(info.value!) " + info.endString!
                    }
                    
                    filteredList.info.append(info)
                }
                
                var checkedPrices = ""
                
                for pr in result["prices"].arrayValue {
                    let price = Price()
                    price.id = pr["id"].intValue
                    price.title = pr["title"].stringValue
                    
                    for subpr in pr["sub_prices"].arrayValue {
                        let subprice = Subprice()
                        subprice.id = subpr["price_id"].intValue
                        subprice.title = subpr["price_title"].stringValue
                        subprice.value = subpr["value"].stringValue
                        price.subprices.append(subprice)
                        
                        if subprice.value != "" {
                            checkedPrices = "\(subprice.title!)"
                        }
                    }
                    if checkedPrices != "" {
                        checkedPrices = checkedPrices.substring(to: checkedPrices.index(checkedPrices.endIndex, offsetBy: -1))
                    }
                    filteredList.checkedPrices = checkedPrices
                    
                    filteredList.prices.append(price)
                }
                
                for ven in result["venues"].arrayValue {
                    let venue = Venue()
                    venue.id = ven["id"].intValue
                    venue.title = ven["title"].stringValue
                    venue.image = Const.URL.domain + ven["image"].stringValue
                    venue.rating = ven["rating"].stringValue
                    venue.catTitle = ven["category_title"].stringValue
                    venue.reviewsCount = ven["reviews_count"].stringValue
                    venue.discount = ven["discount"].intValue
                    filteredList.venues.append(venue)
                }
                
                completionHandler(filteredList, statusCode)
            }
            
        }
    }
}

