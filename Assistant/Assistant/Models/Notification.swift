//
//  Notifications.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 10/30/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import AlamofireDomain
import SwiftyJSON

class Notification {
    
    var id: Int?
    var title: String?
    var description: String?
    var text: String?
    var date: String?
    
    static func fetchNotifications(page: Int, completionHandler: @escaping ([Notification]) -> ()) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true

        let URL = Const.URL.notifications
        
        let token = userDefaults.getAccessToken()
        let headers = ["accessToken": token]
        let params = ["page": "\(page)", "perPage": "10"]
        
        var notifications = [Notification]()
        
        AlamofireDomain.request(URL, method: .get, parameters: params, headers: headers).responseJSON { (response) in
            let statusCode = response.response?.statusCode
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            if statusCode == 200 {
                if let value = response.result.value {
                    let json = JSON(value)

                    for result in json["result"]["notifications"].arrayValue {
                        let notification = Notification()
                        notification.id = result["id"].intValue
                        notification.title = result["title"].stringValue
                        notification.description = result["description"].stringValue
                        notification.text = result["text"].stringValue
                        notification.date = result["date"].stringValue
                        notifications.append(notification)
                    }
                    
                    completionHandler(notifications)
                }
            }
        }
    }
    
    static func fetchNotificationBy(id: Int, completionHandler: @escaping (Notification, Int) -> ()) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        let URL = Const.URL.notificationById
        
        let token = userDefaults.getAccessToken()
        let headers = ["accessToken": token]
        let params = ["notification_id": "\(id)"]
        
        let notification = Notification()
        
        AlamofireDomain.request(URL, method: .get, parameters: params, headers: headers).responseJSON { (response) in
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            let statusCode = response.response?.statusCode ?? 500
            
            if let value = response.result.value {
                let json = JSON(value)
                let result = json["result"]
                
                print("notification detail json", json)
                
                notification.id = result["id"].intValue
                notification.title = result["title"].stringValue
                notification.description = result["description"].stringValue
                notification.text = result["text"].stringValue
                notification.date = result["date"].stringValue
                
                completionHandler(notification, statusCode)
            }
        }
    }
}
