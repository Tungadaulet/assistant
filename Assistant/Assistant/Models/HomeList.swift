//
//  HomeVenue.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 10/19/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import AlamofireDomain
import SwiftyJSON

class HomeList {
    
    var id: Int?
    var title: String?
    var venues = [Venue]()
    
    static func fetchHomeList(completionHandler: @escaping ([HomeList], [Banner], Int) -> ()) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true

        let URL = Const.URL.home
        
        let token = userDefaults.getAccessToken()
        let headers = ["accessToken": token]
        
        var homeList = [HomeList]()
        var banners = [Banner]()
        
        AlamofireDomain.request(URL, method: .get, headers: headers).validate().responseJSON { (response) in

            UIApplication.shared.isNetworkActivityIndicatorVisible = false

            let statusCode = response.response?.statusCode ?? 500
            
            if let value = response.result.value {
                let json = JSON(value)
                
                for result in json["result"]["banner"].arrayValue {
                    let banner = Banner()
                    banner.id = result["id"].intValue
                    banner.title = result["title"].stringValue
                    banner.image = Const.URL.domain + result["image"].stringValue
                    banner.link = result["link"].stringValue
                    banners.append(banner)
                }

                for result in json["result"]["main"].arrayValue {
                    let list = HomeList()
                    list.id = result["id"].intValue
                    list.title = result["title"].stringValue

                    for ven in result["venues"].arrayValue {
                        let venue = Venue()
                        venue.id = ven["id"].intValue
                        venue.title = ven["title"].stringValue
                        venue.image = Const.URL.domain + ven["image"].stringValue
                        venue.rating = ven["rating"].stringValue
                        venue.catTitle = ven["category_title"].stringValue
                        venue.reviewsCount = ven["reviews_count"].stringValue
                        venue.discount = ven["discount"].intValue

                        list.venues.append(venue)
                    }
                    
                    if list.venues.count > 0 {
                        homeList.append(list)
                    }
                }

                completionHandler(homeList, banners, statusCode)
            }
        }
    }
}
