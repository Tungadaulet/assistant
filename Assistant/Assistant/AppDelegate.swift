//
//  AppDelegate.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 9/25/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import MMDrawerController
import GoogleMaps
import UserNotifications
import Firebase
import FirebaseAuth
import InAppNotify

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var menuController: MMDrawerController?

    let mapKey = "AIzaSyAdh6Kawz_e_DkR5NV1BuNJiST9UVRLJ6w"
    let gcmMessageIDKey = "gcm.message_id"

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
//        let token = "uqpXuBYfSGi46I3uJInlTzOEGFx3Mgs0yReTW6a9dgH32EcAxGy8pONpuKPWXb0is2mfRjW1Of6xs4KYjNv5iybfQdm0FgUsyBCFqUqy06leFmW7Oi7bgEGF0O4kL7gnjHqQbH3DLFobKpx3hql7gt"
//        userDefaults.setAccessToken(value: token)
//        userDefaults.setIsLoggedIn(value: true)
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        
        configureNavigationBar()
        setAppRootViewController()
        
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().toolbarDoneBarButtonItemText = "Закрыть"
        
        GMSServices.provideAPIKey(mapKey)
        
        FirebaseApp.configure()
        Messaging.messaging().delegate = self

        registerForRemoteNotifications(application: application)
        
        return true
    }
    
    func initMenuController() {
        let leftSideController = LeftMenuController()
        let centerNavController = UINavigationController(rootViewController: HomeViewController())
        
        menuController = MMDrawerController(center: centerNavController, leftDrawerViewController: leftSideController)
        
        menuController?.shadowRadius = 2
        menuController?.openDrawerGestureModeMask = MMOpenDrawerGestureMode.panningCenterView
        menuController?.closeDrawerGestureModeMask = MMCloseDrawerGestureMode.panningCenterView        
    }
    
    func setAppRootViewController() {
        let isFirstLaunched = userDefaults.isFirstLaunched()
        var rootViewController: UIViewController = StartViewController()
        
        if isFirstLaunched {
            let isLoggedIn = userDefaults.isLoggedIn()
            if isLoggedIn {
                initMenuController()
                rootViewController = menuController!
            }
            else {
                rootViewController = LoginViewController()
            }
        }
        
        window?.rootViewController = rootViewController
        
        if !isFirstLaunched {
            userDefaults.setIsFirstLaunched(value: true)
        }
    }
    
    fileprivate func configureNavigationBar() {
        let barAppearance = UINavigationBar.appearance()
        barAppearance.barTintColor = Const.Colors.navBar
        barAppearance.tintColor = .white
        barAppearance.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        barAppearance.backIndicatorImage = #imageLiteral(resourceName: "back")
        barAppearance.backIndicatorTransitionMaskImage = #imageLiteral(resourceName: "back")
    }
    
    fileprivate func registerForRemoteNotifications(application: UIApplication) {
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
    }
    
    // [START receive_message]
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        showCustomNotificationBanner(userInfo: userInfo)
        
        // Print full message.
        print(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        let notificationId = userInfo["notification_id"] as! String
        notificationClickAction(id: Int(notificationId)!)
                
        if Auth.auth().canHandleNotification(userInfo) {
            completionHandler(UIBackgroundFetchResult.noData)
            return
        }
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    // [END receive_message]
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    // This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
    // If swizzling is disabled then this function must be implemented so that the APNs token can be paired to
    // the FCM registration token.
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("APNs token retrieved: \(deviceToken)")
        
        // With swizzling disabled you must set the APNs token here.
         Messaging.messaging().apnsToken = deviceToken
        
        Auth.auth().setAPNSToken(deviceToken, type: AuthAPNSTokenType.prod)
    }
    
    func showCustomNotificationBanner(userInfo: [AnyHashable: Any]) {
        var title: String? = ""
        var message: String?
        
        guard let aps = userInfo["aps"] as? NSDictionary else {
            return
        }
        if let alert = aps["alert"] as? NSDictionary {
            if let tit = alert["title"] as? String {
                title = tit
            }
            if let body = alert["body"] as? String {
                message = body
            }
        }
        else if let alert = aps["alert"] as? String {
            title = alert
        }
        let notificationId = userInfo["notification_id"] as? String
        
        let announce = Announcement(title: title!, subtitle: message, image: nil, urlImage: nil, duration: 4, interactionType: .none, userInfo: notificationId) { (type, string, annoucement) in
            if type == .tap {
                guard let userInfo = annoucement.userInfo as? String else { return }
                guard let id = Int(userInfo) else { return }
                self.notificationClickAction(id: id)
            }
        }
        InAppNotify.Show(announce, to: (window?.rootViewController)!)
    }
    
    func notificationClickAction(id: Int) {
        let viewController = NotificationsViewController()
        let centerNavController = UINavigationController(rootViewController: viewController)
        menuController?.setCenterView(centerNavController, withCloseAnimation: true, completion: nil)
        let detailViewController = NotificationDetailVC()
        detailViewController.notificationId = id
        centerNavController.pushViewController(detailViewController, animated: true)
    }

}


// [START ios_10_message_handling]
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        showCustomNotificationBanner(userInfo: userInfo)
        
        if Auth.auth().canHandleNotification(userInfo) {
            completionHandler(UNNotificationPresentationOptions.sound)
            return
        }
        
        // Change this to your preferred presentation option
        completionHandler([])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        let notificationId = userInfo["notification_id"] as! String
        notificationClickAction(id: Int(notificationId)!)
        
        if Auth.auth().canHandleNotification(userInfo) {
            completionHandler()
            return
        }
        
        completionHandler()
    }
}
// [END ios_10_message_handling]

extension AppDelegate : MessagingDelegate {
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
    }
    // [END refresh_token]
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    // [END ios_10_data_message]
}

