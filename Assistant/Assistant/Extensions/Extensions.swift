//
//  Extensions.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 9/26/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import UIKit

extension UIColor {
    
    static func rgb(_ red: CGFloat, green: CGFloat, blue: CGFloat) -> UIColor {
        return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: 1)
    }
}

extension UIView {
    
    func addGradient() {
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.cornerRadius = self.layer.cornerRadius
        let firstColor = Const.Colors.menu
        let secondColor = Const.Colors.navBar
        let colors = [firstColor.cgColor, secondColor.cgColor]
        gradient.colors = colors
        gradient.startPoint = CGPoint(x: 0.5, y: 1)
        gradient.endPoint = CGPoint(x: 0.5, y: 0)
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    func roundCorners(corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}

extension UITableViewCell {
    
    override func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
        
        let borderWidth: CGFloat = 1
        
        let leftBorder = getBorderLayer(mask: mask)
        leftBorder.frame = CGRect(x: 0, y: 0, width: borderWidth, height: bounds.height+5)
        
        let bottomBorder = getBorderLayer(mask: mask)
        bottomBorder.frame = CGRect(x: 0, y: bounds.height-borderWidth, width: bounds.width, height: borderWidth)
        
        let rightBorder = getBorderLayer(mask: mask)
        rightBorder.frame = CGRect(x: bounds.width-borderWidth, y: 0, width: borderWidth, height: self.bounds.height)
        
        self.layer.addSublayer(leftBorder)
        self.layer.addSublayer(bottomBorder)
        self.layer.addSublayer(rightBorder)
    }
    
    func getBorderLayer(mask: CAShapeLayer) -> CAShapeLayer {
        let border = CAShapeLayer()
        border.path = mask.path
        border.fillColor = UIColor.clear.cgColor
        border.backgroundColor = Const.Colors.navBar.cgColor
        return border
    }
    
    func getBorderLayer() -> CAShapeLayer {
        let border = CAShapeLayer()
        border.fillColor = UIColor.clear.cgColor
        border.backgroundColor = Const.Colors.navBar.cgColor
        return border
    }
}

extension UIViewController {
    
    func addLeftMenuNavBarItem() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "menu"), style: .plain, target: self, action: #selector(openLeftSideMenu))
    }
    
    func openLeftSideMenu() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.menuController?.toggle(.left, animated: true, completion: nil)
    }
    
    func showAlertWhenGPSDenied() {
        let alert = UIAlertController(title: "Доступ к геолокации запрещён", message: "Пожалуйста, откройте настройки и разрешите Midas использовать ваше местонахождение", preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Закрыть", style: .cancel, handler: nil)
        let settingsAction = UIAlertAction(title: "Настройки", style: .default) { (Void) in
            UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
        }
        alert.addAction(okAction)
        alert.addAction(settingsAction)

        present(alert, animated: true, completion: nil)
    }
    
    func handleError(with statusCode: Int) {
        if statusCode == 401 {
            logout()
        }
        else if statusCode == 408 {
            showErrorAlert(title: "Время запроса истекло", message: "Пожалуйста, повторите попытку еще раз")
        }
        else if statusCode >= 500 {
            showErrorAlert(title: "Ошибка сервера", message: "Пожалуйста, повторите попытку позже")
        }
    }
    
    func logout() {
        userDefaults.setIsLoggedIn(value: false)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.setAppRootViewController()
    }

    func showNetworkErrorAlert() {
        let alert = UIAlertController(title: "Нет интернет-соединения", message: "Данные не могут быть загружены", preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "ОК", style: .cancel, handler: nil)
        
        alert.addAction(okAction)
        
        present(alert, animated: true, completion: nil)
    }
    
    func showErrorAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "ОК", style: .cancel, handler: nil)
        
        alert.addAction(okAction)
        
        present(alert, animated: true, completion: nil)
    }
}

let userDefaults = UserDefaults.standard

extension UserDefaults {
    
    enum UserDefaultsKeys: String {
        case isLoggedIn
        case isFirstLaunched
        case token
        case userId
        case categories
        case authVerificationID
    }
    
    func setIsFirstLaunched(value: Bool) {
        setValue(value, forKey: UserDefaultsKeys.isFirstLaunched.rawValue)
    }
    
    func isFirstLaunched() -> Bool {
        return bool(forKey: UserDefaultsKeys.isFirstLaunched.rawValue)
    }
    
    func setIsLoggedIn(value: Bool) {
        setValue(value, forKey: UserDefaultsKeys.isLoggedIn.rawValue)
    }
    
    func isLoggedIn() -> Bool {
        return bool(forKey: UserDefaultsKeys.isLoggedIn.rawValue)
    }
    
    func setAccessToken(value: String) {
        setValue(value, forKey: UserDefaultsKeys.token.rawValue)
    }
    
    func getAccessToken() -> String {
        return string(forKey: UserDefaultsKeys.token.rawValue)!
    }
    
    func setCategories(value: [Category]) {
        let encodedData = NSKeyedArchiver.archivedData(withRootObject: value)
        set(encodedData, forKey: UserDefaultsKeys.categories.rawValue)
    }
    
    func getCategories() -> [Category]? {
        if let data = object(forKey: UserDefaultsKeys.categories.rawValue) as? Data {
            if let storedData = NSKeyedUnarchiver.unarchiveObject(with: data) as? [Category] {
                return storedData
            }
        }
        
        return nil
    }
    
    func setAuthVerificationID(value: String) {
        setValue(value, forKey: UserDefaultsKeys.authVerificationID.rawValue)
    }
    
    func getAuthVerificationID() -> String {
        return string(forKey: UserDefaultsKeys.authVerificationID.rawValue)!
    }
}
