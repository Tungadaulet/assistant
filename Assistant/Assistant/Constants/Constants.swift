//
//  Constants.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 9/26/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import UIKit

struct Const {
    
    struct Colors {
        static let navBar = UIColor.rgb(40, green: 48, blue: 72)
        static let menu = UIColor.rgb(94, green: 100, blue: 116)    
    }
    
    struct URL {
        static let base = "http://middas.kz/api/v1/"
        static let domain = "http://middas.kz"
        static let categories = base + "categories"
        static let home = base + "main"
        static let venuesByCatId = base + "main/venues"
        static let venue = base + "venue"
        static let reviews = base + "reviews"
        static let addReview = base + "review/add"
        static let login = base + "login"
        static let deleteUser = base + "user/delete"
        static let addFCMToken = base + "push/add"
        static let filteredVenues = base + "venues"
        static let notifications = base + "notifications"
        static let notificationById = base + "notification"
        static let search = base + "search"
    }
}
