//
//  Connectivity.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 11/19/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import AlamofireDomain

class Connectivity {

    class func isConnectedToInternet() -> Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}

