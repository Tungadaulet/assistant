//
//  ReviewsCell.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 10/25/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import UIKit

class ReviewsCell: UITableViewCell {
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = .none
        
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let profileImageView = UIImageView(image: #imageLiteral(resourceName: "avatar"))
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.textColor = Const.Colors.navBar
        label.font = UIFont.boldSystemFont(ofSize: 14)
        return label
    }()
    
    let dateLabel: UILabel = {
        let label = UILabel()
        label.textColor = .lightGray
        label.font = UIFont.systemFont(ofSize: 12)
        label.textAlignment = .right
        return label
    }()
    
    let reviewTextView: UITextView = {
        let tv = UITextView()
        tv.textColor = Const.Colors.navBar.withAlphaComponent(0.5)
        tv.font = UIFont.systemFont(ofSize: 13)
        tv.isScrollEnabled = false
        tv.isEditable = false
        tv.textContainerInset = .zero
        tv.textContainer.lineFragmentPadding = 0
        tv.backgroundColor = .clear
        return tv
    }()
    
    func setupViews() {
        addSubview(profileImageView)
        addSubview(dateLabel)
        addSubview(nameLabel)
        addSubview(reviewTextView)
        
        profileImageView.snp.makeConstraints { (make) in
            make.top.equalTo(10)
            make.left.equalTo(30)
            let width = profileImageView.image?.size.width
            let height = profileImageView.image?.size.height
            make.width.equalTo(width!)
            make.height.equalTo(height!)
        }
        
        dateLabel.snp.makeConstraints { (make) in
            make.top.equalTo(profileImageView).offset(4)
            make.right.equalTo(-30)
            make.width.equalTo(self).multipliedBy(0.2)
        }
        
        nameLabel.snp.makeConstraints { (make) in
            make.bottom.equalTo(dateLabel)
            make.left.equalTo(profileImageView.snp.right).offset(10)
            make.right.equalTo(dateLabel.snp.left).offset(-10)
        }
        
        reviewTextView.snp.makeConstraints { (make) in
            make.top.equalTo(profileImageView.snp.bottom).offset(-16)
            make.left.equalTo(nameLabel)
            make.right.equalTo(dateLabel.snp.centerX)            
            make.bottom.equalTo(-10)
        }
        
    }
}
