//
//  VenueMenuExpandableCell.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 10/24/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import UIKit

class VenueMenuExpandableCell: ExpandableCell {
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setupSeparatorLine() {}
    
    override func configViews() {
        layer.cornerRadius = 4
        selectionStyle = .none
        accessoryView = arrowImageView
        textLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        textLabel?.textColor = .white
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()

        addGradient()
    }
}
