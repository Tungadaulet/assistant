//
//  VenueScheduleCell.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 12/20/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import UIKit

class VenueScheduleCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var weekTimes = [WeekTimes]()
    var days = ["ПН", "ВТ", "СР", "ЧТ", "ПТ", "СБ", "ВС"]
    
    let cellId = "cellId"
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.dataSource = self
        cv.delegate = self
        cv.backgroundColor = .clear
        cv.showsHorizontalScrollIndicator = false
        return cv
    }()
    
    func setupViews() {
        addSubview(collectionView)
        
        collectionView.snp.makeConstraints { (make) in
            make.top.equalTo(8)
            make.left.equalTo(20)
            make.right.equalTo(-20)
            make.height.equalTo(collectionView.snp.width).multipliedBy(0.25)
            make.bottom.equalTo(0)
        }
        
        collectionView.register(ScheduleCell.self, forCellWithReuseIdentifier: cellId)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return weekTimes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! ScheduleCell
        
        cell.backgroundColor = weekTimes[indexPath.item].isToday ? Const.Colors.navBar : .clear
        cell.dayLabel.textColor = weekTimes[indexPath.item].isToday ? .white : Const.Colors.navBar
        cell.timeLabel.textColor = weekTimes[indexPath.item].isToday ? .white : Const.Colors.navBar
        
        cell.dayLabel.text = days[weekTimes[indexPath.item].weekDay!-1]
        
        if weekTimes[indexPath.item].otherTime == "" {
            let openTime = weekTimes[indexPath.item].openTime!
            let closeTime = weekTimes[indexPath.item].closeTime!
            cell.timeLabel.text = openTime + "\n" + closeTime
        }
        else {
            cell.timeLabel.text = weekTimes[indexPath.item].otherTime
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width/7
        let height = collectionView.frame.height
        
        return CGSize(width: width, height: height)
    }
}
