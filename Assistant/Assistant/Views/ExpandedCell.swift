//
//  ExpandedCell.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 10/14/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import UIKit

class ExpandedCell: UITableViewCell {
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        configViews()
        setupSeparatorLine()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let countLabel: UILabel = {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 40, height: 20))
        label.textColor = .white
        label.backgroundColor = Const.Colors.navBar
        label.font = UIFont.systemFont(ofSize: 12)
        label.textAlignment = .center
        label.layer.cornerRadius = 10
        label.clipsToBounds = true
        return label
    }()
    
    func configViews() {
        backgroundColor = Const.Colors.menu
        textLabel?.font = UIFont.systemFont(ofSize: 14)
        textLabel?.textColor = .white
        accessoryView = countLabel
    }
    
    func setupSeparatorLine() {
        let line = CALayer()
        line.backgroundColor = Const.Colors.navBar.cgColor
        line.frame = CGRect(x: 0, y: 43, width: self.frame.size.width, height: 1)
        layer.addSublayer(line)
    }

}
