//
//  VenueFiltersCell.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 10/21/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import UIKit
import SnapKit

class VenueFiltersCell: UITableViewCell {
    
    var filters = [VenueFilter]() {
        didSet {
            selectionStyle = .none
            
            if filters.count > 0 {
                setupViews()
            }
        }
    }
    
    let borderView: UIView = {
        let view = UIView()
        view.layer.borderColor = Const.Colors.navBar.cgColor
        view.layer.borderWidth = 1
        view.layer.cornerRadius = 4
        view.clipsToBounds = true
        return view
    }()
    
    func removeAllSubviews() {
        for view in subviews {
            for subview in view.subviews {
                subview.removeFromSuperview()
            }
            view.removeFromSuperview()
        }
    }
    
    func setupViews() {
        removeAllSubviews()
        
        addSubview(borderView)
        
        for (index, filter) in filters.enumerated() {
            let isMarked = filter.isMarked
            
            let label = UILabel()
            label.numberOfLines = 0
            
            let attributedString = NSMutableAttributedString(string: filter.title! + "\n", attributes: [NSFontAttributeName: UIFont.boldSystemFont(ofSize: 13), NSForegroundColorAttributeName: isMarked ? UIColor.white : Const.Colors.navBar])
            attributedString.append(NSAttributedString(string: filter.valueString!, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 13), NSForegroundColorAttributeName: isMarked ? UIColor.white : Const.Colors.navBar]))
            label.attributedText = attributedString
            
            let line = UIView()
            line.backgroundColor = Const.Colors.navBar
            
            let bgView = UIView()
            bgView.backgroundColor = isMarked ? UIColor.rgb(233, green: 150, blue: 255) : UIColor.clear
            
            let subviews = borderView.subviews
            
            borderView.addSubview(bgView)
            borderView.addSubview(label)
            borderView.addSubview(line)
            
            label.snp.makeConstraints({ (make) in
                if index == 0 {
                    make.top.equalTo(8)
                }
                else {
                    make.top.equalTo(subviews[subviews.count-1].snp.bottom).offset(8)
                }
                make.left.equalTo(8)
                make.right.equalTo(-8)
                
                if index == filters.count-1 {
                    make.bottom.equalTo(self).offset(-8)
                    
                    borderView.snp.makeConstraints { (make) in
                        make.top.equalTo(10)
                        make.left.equalTo(20)
                        make.right.equalTo(-20)
                        make.bottom.equalTo(label).offset(8)
                    }
                }
            })
            
            bgView.snp.makeConstraints({ (make) in
                make.top.left.equalTo(label).offset(-8)
                make.bottom.right.equalTo(label).offset(8)
            })
            
            if index != filters.count-1 {
                line.snp.makeConstraints({ (make) in
                    make.top.equalTo(bgView.snp.bottom)
                    make.left.right.equalTo(borderView)
                    make.height.equalTo(1)
                })
            }
            
        }
    }
    
}
