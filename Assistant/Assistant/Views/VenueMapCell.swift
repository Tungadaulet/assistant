//
//  VenueMapCell.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 10/23/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import UIKit
import GoogleMaps

class VenueMapCell: UITableViewCell {
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = .none
        
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var latitude: Double?
    var longitude: Double? {
        didSet {
            if let lat = self.latitude, let lng = self.longitude {
                self.mapView.clear()
                
                let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: lng, zoom: 13.0)
                self.mapView.camera = camera
                let marker = GMSMarker()
                marker.position = CLLocationCoordinate2D(latitude: lat, longitude: lng)
                marker.map = self.mapView
                marker.icon = #imageLiteral(resourceName: "marker")
            }
        }
    }
    
    let mapView = GMSMapView()
    
    let gradientView = UIView()
    
    let showLabel: UILabel = {
        let label = UILabel()
        label.text = "Показать на карте"
        label.textColor = .white
        label.textAlignment = .center
        return label
    }()
    
    func setupViews() {
        addSubview(mapView)
        addSubview(gradientView)
        addSubview(showLabel)
        
        mapView.snp.makeConstraints { (make) in
            make.top.equalTo(10)
            make.left.bottom.right.equalTo(0)
            make.height.equalTo(mapView.snp.width).multipliedBy(0.5)
        }
        
        gradientView.snp.makeConstraints { (make) in
            make.edges.equalTo(mapView)
        }
        
        showLabel.snp.makeConstraints { (make) in
            make.edges.equalTo(mapView)
        }
        
        let almaty = GMSCameraPosition.camera(withLatitude: 43.220641, longitude: 76.85146, zoom: 12.0)
        mapView.camera = almaty
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if let sublayers = gradientView.layer.sublayers, sublayers.count == 1 {
            return
        }
        
        let gradient = CAGradientLayer()
        gradient.frame = gradientView.bounds
        let firstColor = UIColor.clear
        let secondColor = Const.Colors.navBar.withAlphaComponent(0.6)
        let colors = [firstColor.cgColor, secondColor.cgColor]
        gradient.colors = colors
        gradient.startPoint = CGPoint(x: 0.5, y: 1)
        gradient.endPoint = CGPoint(x: 0.5, y: 0)
        gradientView.layer.insertSublayer(gradient, at: 0)
    }
}
