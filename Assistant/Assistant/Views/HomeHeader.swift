//
//  HomeHeader.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 11/6/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import UIKit
import FSPagerView
import SafariServices

class HomeHeader: UITableViewHeaderFooterView, FSPagerViewDataSource, FSPagerViewDelegate, UICollectionViewDelegateFlowLayout, SFSafariViewControllerDelegate {
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        
        contentView.backgroundColor = .clear
        
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var banners = [Banner]() {
        didSet {
            bannerView.reloadData()
        }
    }
    
    let bannerView: FSPagerView = {
        let view = FSPagerView()
        view.backgroundColor = .clear
        view.interitemSpacing = 0
        view.isInfinite = true
        view.automaticSlidingInterval = 5
        return view
    }()
    
    let cellId = "cellId"
    
    func setupViews() {
        addSubview(bannerView)
        
        bannerView.snp.makeConstraints { (make) in
            make.top.left.bottom.right.equalTo(0)
            make.height.equalTo(self.snp.width).multipliedBy(0.5)
        }
        
        bannerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: cellId)
        
        bannerView.dataSource = self
        bannerView.delegate = self
        
        let itemWidth = self.frame.width
        let itemHeight = self.frame.height
        bannerView.itemSize = CGSize(width: itemWidth, height: itemHeight)
    }
    
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return banners.count
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: cellId, at: index)
        
        let url = URL(string: banners[index].image!)
        cell.imageView?.kf.setImage(with: url, options: [.transition(.fade(0.2))])
        cell.imageView?.contentMode = .scaleAspectFill
        cell.imageView?.clipsToBounds = true
        
        return cell
    }
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        guard let link = banners[index].link, let url = URL(string: link) else {
            return
        }
        let canOpenURL = UIApplication.shared.canOpenURL(url)
        if !canOpenURL {
            return
        }
        
        if #available(iOS 9.0, *) {
            let svc = SFSafariViewController(url: url)
            svc.delegate = self
            guard let window = UIApplication.shared.keyWindow, let rootVC = window.rootViewController else {
                openSafari(url: url)
                return
            }
            UIApplication.shared.statusBarStyle = .default
            rootVC.present(svc, animated: true, completion: nil)
        } else {
            openSafari(url: url)
        }
    }
    
    func openSafari(url: URL) {
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    @available(iOS 9.0, *)
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
}
