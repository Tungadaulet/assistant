//
//  MenuHeader.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 9/28/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import UIKit

class MenuHeaderCell: UITableViewCell {
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = Const.Colors.menu
        selectionStyle = .none
        
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let logoImageView = UIImageView(image: #imageLiteral(resourceName: "header-midas"))
    let discountImageView = UIImageView(image: #imageLiteral(resourceName: "header-discount"))
    
    let discountLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.text = "Скидки"
        label.font = UIFont.systemFont(ofSize: 14)
        return label
    }()
    
    let taglineLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 14)
        label.text = "Экономика заключается в экономии!"
        label.numberOfLines = 0
        return label
    }()
    
    func setupViews() {
        addSubview(logoImageView)
        addSubview(discountLabel)
        addSubview(discountImageView)
        addSubview(taglineLabel)
        
        logoImageView.snp.makeConstraints { (make) in
            make.top.equalTo(16)
            make.left.equalTo(12)
        }
        
        discountLabel.snp.makeConstraints { (make) in
            make.top.equalTo(logoImageView.snp.bottom).offset(10)
            make.left.equalTo(logoImageView)
        }
        
        discountImageView.snp.makeConstraints { (make) in
            make.centerY.equalTo(discountLabel)
            make.left.equalTo(discountLabel.snp.right).offset(6)
        }
        
        taglineLabel.snp.makeConstraints { (make) in
            make.top.equalTo(discountImageView.snp.bottom).offset(6)
            make.left.equalTo(logoImageView)
            make.bottom.equalTo(-10)
        }
    }
    
    
}
