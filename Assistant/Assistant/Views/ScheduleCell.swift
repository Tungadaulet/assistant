//
//  ScheduleCell.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 12/20/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import UIKit

class ScheduleCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        layer.cornerRadius = 3
        
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let dayLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 13)
        label.textColor = Const.Colors.navBar
        return label
    }()

    let timeLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = Const.Colors.navBar
        label.numberOfLines = 0
        return label
    }()
    
    func setupViews() {
        addSubview(dayLabel)
        addSubview(timeLabel)
        
        dayLabel.snp.makeConstraints { (make) in
            make.top.equalTo(4)
            make.left.right.equalTo(0)
        }
        
        timeLabel.snp.makeConstraints { (make) in
            make.left.right.equalTo(0)
            make.bottom.equalTo(-6)
        }
    }
}
