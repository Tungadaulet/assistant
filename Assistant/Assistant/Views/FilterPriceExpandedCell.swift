//
//  FilterPriceExpandedCell.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 12/5/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import UIKit

class FilterPriceExpandedCell: VenueMenuExpandedCell {
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let priceButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = Const.Colors.navBar
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        button.titleLabel?.textAlignment = .center
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        button.layer.cornerRadius = 10
        button.clipsToBounds = true
        return button
    }()
    
    //let checkImageView = UIImageView(image: #imageLiteral(resourceName: "unchecked"))
    
    let accessoryViews = UIView(frame: CGRect(x: 0, y: 0, width: 60, height: 20))
    
    override func configViews() {
        textLabel?.font = UIFont.systemFont(ofSize: 12)
        textLabel?.textColor = Const.Colors.navBar
        textLabel?.numberOfLines = 0
        accessoryView = priceButton
    }
    
    override func setupViews() {
        priceButton.frame = CGRect(x: 0, y: 0, width: 50, height: 20)
    }
}
