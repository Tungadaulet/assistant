//
//  FilteredVenuesCell.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 10/26/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import UIKit

class FilteredVenuesCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        layer.borderColor = Const.Colors.navBar.cgColor
        layer.borderWidth = 1
        layer.cornerRadius = 4
        
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let label: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 13)
        return label
    }()
    
    let arrowImageView = UIImageView(image: #imageLiteral(resourceName: "arrow-down"))
    
    func setupViews() {
        addSubview(arrowImageView)
        addSubview(label)
        
        arrowImageView.snp.makeConstraints { (make) in
            make.centerY.equalTo(self)
            make.right.equalTo(-12)            
        }
        
        let arrowImage = #imageLiteral(resourceName: "arrow-down")
        
        label.snp.makeConstraints { (make) in
            make.top.bottom.equalTo(0)
            make.left.equalTo(8)
            let rightOffset = arrowImage.size.width+12
            make.right.equalTo(-(rightOffset))
        }
    }
}
