//
//  HomeCell.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 9/27/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import UIKit
import Kingfisher

class HomeCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = .none
        
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var venues = [Venue]() {
        didSet {
            collectionView.reloadData()
        }
    }
    var homeDelegate: HomeDelegate?
    
    let categoryLabel: UILabel = {
        let label = UILabel()
        label.text = "Заведения"
        label.textColor = Const.Colors.navBar
        label.font = UIFont.boldSystemFont(ofSize: 15)
        return label
    }()
    
    let seeAllBtn: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Посмотреть >", for: .normal)
        button.setTitleColor(Const.Colors.navBar, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        return button
    }()
    
    lazy var collectionView: UICollectionView = {
        let layout = SnappingCollectionViewLayout()
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.dataSource = self
        cv.delegate = self
        cv.showsVerticalScrollIndicator = false
        cv.showsHorizontalScrollIndicator = false
        cv.backgroundColor = .clear
        cv.contentInset = UIEdgeInsetsMake(0, 8, 0, 8)
        cv.decelerationRate = UIScrollViewDecelerationRateFast
        return cv
    }()
    
    let cellId = "cellId"
    
    func setupViews() {
        addSubview(categoryLabel)
        addSubview(seeAllBtn)
        addSubview(collectionView)
        
        categoryLabel.snp.makeConstraints { (make) in
            make.top.equalTo(12)
            make.left.equalTo(8)
        }
        
        seeAllBtn.snp.makeConstraints { (make) in
            make.centerY.equalTo(categoryLabel)
            make.right.equalTo(-8)
        }
        
        collectionView.snp.makeConstraints { (make) in
            make.top.equalTo(categoryLabel.snp.bottom).offset(6)
            make.left.right.equalTo(0)
            make.height.equalTo(self.snp.width).multipliedBy(0.31)
            make.bottom.equalTo(0)
        }
        
        collectionView.register(HomeSubcatCell.self, forCellWithReuseIdentifier: cellId)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return venues.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! HomeSubcatCell
        
        cell.reviewsCountLabel.text = venues[indexPath.item].reviewsCount
        cell.ratingLabel.text = venues[indexPath.item].rating
        cell.discountLabel.text = "-\(venues[indexPath.item].discount!)%"
        cell.subcatLabel.text = venues[indexPath.item].catTitle
        cell.nameLabel.text = venues[indexPath.item].title
        
        let url = URL(string: venues[indexPath.item].image!)
        cell.imageView.kf.setImage(with: url, options: [.transition(.fade(0.2))])
        
        cell.discountImageView.isHidden = venues[indexPath.item].discount == 0
        cell.discountLabel.isHidden = cell.discountImageView.isHidden
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = collectionView.frame.height
        let width = height*1.21
        
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let venueId = venues[indexPath.item].id!
        let catTitle = venues[indexPath.item].catTitle!
        
        homeDelegate?.didSelectVenue(id: venueId, catTitle: catTitle)
    }
}
