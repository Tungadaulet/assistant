//
//  VenueSliderCell.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 10/20/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import UIKit
import SKPhotoBrowser

class VenueSliderCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = .none
        
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var images = [String]() {
        didSet {
            collectionView.reloadData()
        }
    }
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.dataSource = self
        cv.delegate = self
        cv.isPagingEnabled = true
        cv.backgroundColor = .clear
        return cv
    }()
    
    let discountImageView = UIImageView(image: #imageLiteral(resourceName: "discount-large"))
    
    let discountLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.text = "-25%"
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.adjustsFontSizeToFitWidth = true
        label.textAlignment = .center
        return label
    }()
    
    let cellId = "cellId"
    
    let leftButton = VenueSliderCell.createSlideButton(withImage: #imageLiteral(resourceName: "slide-left"))
    let rightButton = VenueSliderCell.createSlideButton(withImage: #imageLiteral(resourceName: "slide-right"))
    
    static func createSlideButton(withImage image: UIImage) -> UIButton {
        let button = UIButton(type: .system)
        button.setImage(image, for: .normal)
        button.tintColor = .white
        button.layer.shadowColor = UIColor.black.cgColor
        button.layer.shadowOffset = .zero
        button.layer.shadowRadius = 6
        button.layer.shadowOpacity = 1
        return button
    }
    
    func setupViews() {
        addSubview(collectionView)
        addSubview(discountImageView)
        addSubview(discountLabel)
        addSubview(leftButton)
        addSubview(rightButton)
        
        collectionView.snp.makeConstraints { (make) in
            make.top.left.bottom.right.equalTo(0)
            make.height.equalTo(collectionView.snp.width).multipliedBy(0.55)
        }
        
        discountImageView.snp.makeConstraints { (make) in
            make.top.equalTo(12)
            make.right.equalTo(-24)
        }
        
        discountLabel.snp.makeConstraints { (make) in
            make.edges.equalTo(discountImageView)
        }
        
        leftButton.snp.makeConstraints { (make) in
            make.centerY.equalTo(collectionView)
            make.left.equalTo(12)
        }
        
        rightButton.snp.makeConstraints { (make) in
            make.centerY.equalTo(collectionView)
            make.right.equalTo(-12)
        }
        
        collectionView.register(SliderCell.self, forCellWithReuseIdentifier: cellId)
        
        leftButton.addTarget(self, action: #selector(slideLeft(sender:)), for: .touchUpInside)
        rightButton.addTarget(self, action: #selector(slideRight(sender:)), for: .touchUpInside)
        
    }
    
    func slideLeft(sender: UIButton) {
        let previousPage = sender.tag-1
        
        if previousPage < 0 {
            return
        }
        
        scrollTo(item: previousPage)
        
        leftButton.tag = previousPage
        rightButton.tag = previousPage
    }
    
    func slideRight(sender: UIButton) {
        let nextPage = sender.tag+1
        
        if nextPage > images.count-1 {
            return
        }
        
        scrollTo(item: nextPage)
        
        leftButton.tag = nextPage
        rightButton.tag = nextPage
    }
    
    func scrollTo(item: Int) {
        let indexPath = IndexPath(item: item, section: 0)
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! SliderCell
        
        let url = URL(string: images[indexPath.item])
        cell.imageView.kf.setImage(with: url, options: [.transition(.flipFromTop(0.5))])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.frame.size
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        let pageNumber = Int(targetContentOffset.pointee.x/(self.frame.width))
        
        leftButton.tag = pageNumber
        rightButton.tag = pageNumber
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var photos = [SKPhoto]()
        
        for url in images {
            let photo = SKPhoto.photoWithImageURL(url)
            photo.shouldCachePhotoURLImage = true
            photos.append(photo)
        }
        
        let browser = SKPhotoBrowser(photos: photos)
        browser.initializePageIndex(indexPath.item)
        
        if let window = UIApplication.shared.keyWindow {
            window.rootViewController?.present(browser, animated: true, completion: nil)
        }
    }
}
