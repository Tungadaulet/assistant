//
//  VenueAdditionalInfo.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 10/21/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import UIKit

class VenueAdditionalInfoCell: VenueFiltersCell {
    
    var phones = [String]()
    
    override var filters: [VenueFilter] {
        didSet {
        }
    }
    
    var venueInfo = [VenueInfo]() {
        didSet {
            selectionStyle = .none
            
            if venueInfo.count > 0 {        
                setupViews()
            }
        }
    }
    
    override func setupViews() {
        removeAllSubviews()
        
        addSubview(borderView)
        
        for (index, info) in venueInfo.enumerated() {
            let isMarked = info.isMarked
            
            let label = UILabel()
            label.numberOfLines = 0
            label.font = UIFont.boldSystemFont(ofSize: 13)
            label.text = info.title
            
            let detailLabel = UILabel()
            detailLabel.numberOfLines = 0
            detailLabel.font = UIFont.systemFont(ofSize: 13)
            detailLabel.text = info.value
            detailLabel.isUserInteractionEnabled = true
            
            let view = UIView()
            let detailView = UIView()
            
            let phoneTouchView = UIView()
            phoneTouchView.tag = index
            let linkTap = UITapGestureRecognizer(target: self, action: #selector(linkTapped(sender:)))
            phoneTouchView.addGestureRecognizer(linkTap)
            
            let verticalLine = UIView()
            verticalLine.backgroundColor = .lightGray
            
            let horizontalLine = UIView()
            horizontalLine.backgroundColor = Const.Colors.navBar
            
            if isMarked {
                view.backgroundColor = UIColor.rgb(233, green: 150, blue: 255)
                detailView.backgroundColor = UIColor.rgb(233, green: 150, blue: 255)
                label.textColor = .white
                detailLabel.textColor = .white
            } else {
                view.backgroundColor = UIColor.clear
                detailView.backgroundColor = UIColor.clear
                label.textColor = Const.Colors.navBar
                detailLabel.textColor = (index == 1) ? UIColor.rgb(0, green: 100, blue: 0) : Const.Colors.navBar
            }
            
            guard let title = info.title else { return }
            guard let value = info.value else { return }
            let titleCount = title.characters.count
            let valueCount = value.characters.count
            let isValueLarger = valueCount > titleCount
            
            let subviews = borderView.subviews
            
            borderView.addSubview(verticalLine)
            borderView.addSubview(view)
            borderView.addSubview(label)
            borderView.addSubview(detailView)
            borderView.addSubview(detailLabel)
            borderView.addSubview(phoneTouchView)
            borderView.addSubview(horizontalLine)
            
            verticalLine.snp.makeConstraints({ (make) in
                if index == 0 {
                    make.top.equalTo(0)
                }
                else {
                    make.top.equalTo(subviews[subviews.count-1].snp.bottom)
                }
                make.centerX.equalTo(borderView).offset(-20)
                make.width.equalTo(1)
                if index == venueInfo.count-1 {
                    make.bottom.equalTo(borderView)
                }
                else {
                    make.bottom.equalTo(horizontalLine.snp.top)
                }
            })
            
            view.snp.makeConstraints({ (make) in
                if index == 0 {
                    make.top.equalTo(0)
                }
                else {
                    make.top.equalTo(subviews[subviews.count-1].snp.bottom)
                }
                make.left.equalTo(0)
                make.right.equalTo(verticalLine.snp.left)
                if index == venueInfo.count-1 {
                    make.bottom.equalTo(self).offset(0)
                }
                else {
                    make.bottom.equalTo(horizontalLine.snp.top)
                }
            })
            
            label.snp.makeConstraints({ (make) in
                if index == 0 {
                    make.top.equalTo(8)
                }
                else {
                    make.top.equalTo(subviews[subviews.count-1].snp.bottom).offset(8)
                }
                make.left.equalTo(6)
                make.right.equalTo(verticalLine.snp.left).offset(-6)
                if index == venueInfo.count-1  {
                    make.bottom.equalTo(self).offset(-8)
                    
                    if !isValueLarger {
                        borderView.snp.makeConstraints { (make) in
                            make.top.equalTo(10)
                            make.left.equalTo(20)
                            make.right.equalTo(-20)
                            make.bottom.equalTo(label).offset(8)
                        }
                    }                    
                }
                else {
                    if isValueLarger {
                        make.bottom.equalTo(horizontalLine.snp.top).offset(-8)
                    }
                }
            })
            
            detailView.snp.makeConstraints({ (make) in
                if index == 0 {
                    make.top.equalTo(0)
                }
                else {
                    make.top.equalTo(subviews[subviews.count-1].snp.bottom)
                }
                make.left.equalTo(verticalLine.snp.right)
                make.right.equalTo(0)
                if index == venueInfo.count-1 {
                    make.bottom.equalTo(self).offset(0)
                }
                else {
                    make.bottom.equalTo(horizontalLine.snp.top)
                }
            })
            
            detailLabel.snp.makeConstraints({ (make) in
                make.top.equalTo(label)
                make.left.equalTo(verticalLine.snp.right).offset(6)
                make.right.equalTo(-6)
                if index == venueInfo.count-1 {
                    make.bottom.equalTo(self).offset(-8)
                    
                    if isValueLarger {
                        borderView.snp.makeConstraints { (make) in
                            make.top.equalTo(10)
                            make.left.equalTo(20)
                            make.right.equalTo(-20)
                            make.bottom.equalTo(detailLabel).offset(8)
                        }
                    }
                }
                else {
                    if !isValueLarger {
                        make.bottom.equalTo(horizontalLine.snp.top).offset(-8)
                    }
                }
            })
            
            phoneTouchView.snp.makeConstraints({ (make) in
                make.edges.equalTo(detailView)
            })
            
            if index != venueInfo.count-1 {
                horizontalLine.snp.makeConstraints({ (make) in
                    if isValueLarger {
                        make.top.equalTo(detailLabel.snp.bottom).offset(8)
                    }
                    else {
                        make.top.equalTo(label.snp.bottom).offset(8)
                    }
                    make.left.right.equalTo(0)
                    make.height.equalTo(1)
                })
            }
        }
    }
    
    func linkTapped(sender: UITapGestureRecognizer) {
        guard let tag = sender.view?.tag else {
            return
        }
        
        if phones.count == 0 || tag != 1 {
            return
        }
        if phones.count > 1 {
            showPhonesActionSheet()
        }
        else {
            callTo(phone: phones[0])
        }
    }
    
    func showPhonesActionSheet() {
        let alert = UIAlertController(title: "Выберите номер телефона", message: nil, preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        
        for phone in phones {
            let action = UIAlertAction(title: phone, style: .default, handler: { (Void) in
                self.callTo(phone: phone)
            })
            
            alert.addAction(action)
        }
        
        if let window = UIApplication.shared.keyWindow {
            window.rootViewController?.present(alert, animated: true, completion: nil)
        }
    }
    
    func callTo(phone: String) {
        if let url = URL(string: "tel://\(phone)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
}
