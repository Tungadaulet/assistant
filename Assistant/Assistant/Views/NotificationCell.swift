//
//  NotificationCell.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 10/30/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        return label
    }()
    
    let dateImageView = UIImageView(image: #imageLiteral(resourceName: "calendar"))
    
    let dateLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 11)
        return label
    }()
    
    let textView: UITextView = {
        let tv = UITextView()
        tv.textColor = .black
        tv.font = UIFont.systemFont(ofSize: 13)
        tv.isScrollEnabled = false
        tv.isEditable = false
        tv.isSelectable = false
        tv.textContainerInset = .zero
        tv.textContainer.lineFragmentPadding = 0
        tv.backgroundColor = .clear
        tv.isUserInteractionEnabled = false
        return tv
    }()
    
    func setupViews() {
        addSubview(titleLabel)
        addSubview(dateImageView)
        addSubview(dateLabel)
        addSubview(textView)
        
        titleLabel.snp.makeConstraints { (make) in
            make.top.left.equalTo(12)
            make.right.equalTo(-12)
        }
        
        dateImageView.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(8)
            make.left.equalTo(12)
        }
        
        dateLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(dateImageView)
            make.left.equalTo(dateImageView.snp.right).offset(8)
        }
        
        textView.snp.makeConstraints { (make) in
            make.top.equalTo(dateLabel.snp.bottom).offset(8)
            make.left.equalTo(12)
            make.bottom.right.equalTo(-12)
        }
    }
}
