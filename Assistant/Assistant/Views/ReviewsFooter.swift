//
//  ReviewsFooter.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 10/25/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import UIKit

class ReviewsFooter: UITableViewHeaderFooterView {
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let moreReviewsBtn = InfoViewController.createGradientButton(withTitle: " Еще отзывы", image: #imageLiteral(resourceName: "re"))
    
    func setupViews() {
        addSubview(moreReviewsBtn)
        
        moreReviewsBtn.snp.makeConstraints { (make) in
            make.top.equalTo(10)
            make.left.equalTo(60)
            make.right.equalTo(-60)
            make.height.equalTo(40)
            make.bottom.equalTo(-10)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        moreReviewsBtn.addGradient()
    }
}
