//
//  VenueReviewsCell.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 10/23/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import UIKit

class VenueReviewsCell: UITableViewCell {
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
     
        selectionStyle = .none
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let reviewsButton = InfoViewController.createGradientButton(withTitle: " Отзывы ()", image: #imageLiteral(resourceName: "price"))
    
    func setupViews() {
        addSubview(reviewsButton)
        
        reviewsButton.snp.makeConstraints { (make) in
            make.top.equalTo(10)
            make.left.equalTo(20)
            make.right.equalTo(-20)
            make.height.equalTo(40)
            make.bottom.equalTo(0)
        }
        
        reviewsButton.layer.cornerRadius = 4
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        reviewsButton.addGradient()
    }
}
