//
//  VenueMenuCell.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 10/21/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import UIKit

class VenueMenuCell: UITableViewCell {
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = .none
        
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let menuButton = InfoViewController.createGradientButton(withTitle: "", image: #imageLiteral(resourceName: "price"))
    
    func setupViews() {
        addSubview(menuButton)
        
        menuButton.snp.makeConstraints { (make) in
            make.top.equalTo(10)
            make.left.equalTo(60)
            make.right.equalTo(-60)
            make.height.equalTo(40)
            make.bottom.equalTo(0)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        menuButton.addGradient()
    }
    
    
}
