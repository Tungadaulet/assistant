//
//  ExpandableCell.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 10/14/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import UIKit
import ExpyTableView

class ExpandableCell: UITableViewCell, ExpyTableViewHeaderCell {
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        configViews()
        setupSeparatorLine()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let arrowImageView = UIImageView(image: #imageLiteral(resourceName: "expand-arrow-white"))
    
    func configViews() {
        backgroundColor = Const.Colors.navBar
        textLabel?.font = UIFont.systemFont(ofSize: 14)
        textLabel?.textColor = .white
    }
    
    func setupSeparatorLine() {
        let line = CALayer()
        line.backgroundColor = UIColor.lightGray.cgColor
        line.frame = CGRect(x: 0, y: 59, width: self.frame.size.width, height: 1)
        layer.addSublayer(line)
    }
    
    func changeState(_ state: ExpyState, cellReuseStatus cellReuse: Bool) {
        switch state {
        case .willExpand:
            open()
        case .willCollapse:
            close()
        default:
            break
        }
    }
    
    func open() {
        UIView.animate(withDuration: (0.3)) { [weak self] _ in
            self?.arrowImageView.transform = CGAffineTransform(rotationAngle: (CGFloat.pi))
        }
    }
    
    func close() {
        UIView.animate(withDuration: (0.3)) { [weak self] _ in
            self?.arrowImageView.transform = CGAffineTransform(rotationAngle: 0)
        }
    }
    

}
