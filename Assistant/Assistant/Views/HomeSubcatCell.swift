//
//  HomeSubcatCell.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 9/28/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import UIKit

class HomeSubcatCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        layer.cornerRadius = 4
        
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let imageView: UIImageView = {
        let iv = UIImageView()
        iv.backgroundColor = Const.Colors.navBar
        iv.layer.cornerRadius = 4
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        return iv
    }()
    
    let imageMaskView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0.5)
        view.layer.cornerRadius = 4
        return view
    }()
    
    let reviewImageView = UIImageView(image: #imageLiteral(resourceName: "comment"))
    
    let reviewsCountLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 10)
        label.text = "17"
        return label
    }()
    
    let ratingImageView = UIImageView(image: #imageLiteral(resourceName: "hearth"))
    
    let ratingLabel: UILabel = {
        let label = UILabel()
        label.text = "4"
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 10)
        return label
    }()
    
    let discountImageView = UIImageView(image: #imageLiteral(resourceName: "discount"))
    
    let discountLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.text = "-25%"
        label.font = UIFont.boldSystemFont(ofSize: 7)
        label.adjustsFontSizeToFitWidth = true
        label.textAlignment = .center
        return label
    }()
    
    let subcatLabel: UILabel = {
        let label = UILabel()
        label.text = "Рестораны"
        label.font = UIFont.systemFont(ofSize: 11)
        label.textColor = .lightGray
        return label
    }()
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.text = "Cafe Central"
        label.font = UIFont.boldSystemFont(ofSize: 13)
        return label
    }()
    
    func setupViews() {
        addSubview(imageView)
        addSubview(imageMaskView)
        addSubview(reviewImageView)
        addSubview(reviewsCountLabel)
        addSubview(ratingImageView)
        addSubview(ratingLabel)
        addSubview(discountImageView)
        addSubview(discountLabel)
        addSubview(subcatLabel)
        addSubview(nameLabel)
        
        imageView.snp.makeConstraints { (make) in
            make.edges.equalTo(0)
        }
        
        imageMaskView.snp.makeConstraints { (make) in
            make.edges.equalTo(0)
        }
        
        reviewImageView.snp.makeConstraints { (make) in
            make.top.left.equalTo(8)
        }
        
        reviewsCountLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(reviewImageView)
            make.left.equalTo(reviewImageView.snp.right).offset(2)
        }
        
        ratingImageView.snp.makeConstraints { (make) in
            make.top.equalTo(reviewImageView)
            make.left.equalTo(reviewsCountLabel.snp.right).offset(6)
        }
        
        ratingLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(ratingImageView)
            make.left.equalTo(ratingImageView.snp.right).offset(2)
        }
        
        discountImageView.snp.makeConstraints { (make) in
            make.top.equalTo(6)
            make.right.equalTo(-8)
        }
        
        discountLabel.snp.makeConstraints { (make) in
            make.edges.equalTo(discountImageView)
        }
        
        nameLabel.snp.makeConstraints { (make) in
            make.left.equalTo(8)
            make.bottom.right.equalTo(-4)
        }
        
        subcatLabel.snp.makeConstraints { (make) in
            make.bottom.equalTo(nameLabel.snp.top)
            make.left.right.equalTo(nameLabel)
        }
    }
}
