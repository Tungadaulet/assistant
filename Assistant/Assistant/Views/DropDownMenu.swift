//
//  DropDownMenu.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 11/13/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import UIKit
import SnapKit

protocol DropDownMenuDelegate {
    
    func didSelectItemAt(index: Int)
}

class DropDownMenu: NSObject, UITableViewDataSource, UITableViewDelegate {
    
    var delegate: DropDownMenuDelegate?
    var containerView = UIView()
    
    var tableViewHeight: CGFloat = 0
    
    var items = [String]() {
        didSet {
            let rowHeight = 44
            tableViewHeight = CGFloat(items.count * rowHeight)
            if tableViewHeight > containerView.frame.height {
                tableViewHeight = containerView.frame.height
                tableView.isScrollEnabled = true
            }
        }
    }
    
    var isHidden = true
    var selectedItem = 0
    
    lazy var tableView: UITableView = {
        let tv = UITableView()
        tv.dataSource = self
        tv.delegate = self
        tv.separatorInset = .zero
        tv.tableFooterView = UIView()
        tv.isScrollEnabled = false
        return tv
    }()
    
    let cellId = "cellId"
    
    var blackView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0.5)
        return view
    }()
    
    func show() {
        if items.count == 0 {
            return
        }
        self.isHidden = false
        
        containerView.addSubview(blackView)
        containerView.addSubview(tableView)
        
        blackView.frame = containerView.frame
        blackView.alpha = 0
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismiss))
        blackView.addGestureRecognizer(tap)
        
        tableView.frame = CGRect(x: 0, y: 64-tableViewHeight, width: containerView.frame.width, height: tableViewHeight)
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellId)
        
        UIView.animate(withDuration: 0.5 , delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            
            self.tableView.frame.origin.y = 64
            self.blackView.alpha = 1
            
        }, completion: nil)
    }
    
    func dismiss() {
        self.isHidden = true

        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            
            self.tableView.frame.origin.y = 64-self.tableViewHeight
            self.blackView.alpha = 0
            
        }, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        
        cell.textLabel?.textColor = Const.Colors.navBar
        cell.textLabel?.font = UIFont.systemFont(ofSize: 14)
        cell.textLabel?.text = items[indexPath.row]
       
        let isSelected = selectedItem == indexPath.row
        cell.accessoryView = UIImageView(image: isSelected ? #imageLiteral(resourceName: "check-mark") : UIImage())
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedItem = indexPath.row
        self.delegate?.didSelectItemAt(index: indexPath.row)
        tableView.reloadData()
        dismiss()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
}
