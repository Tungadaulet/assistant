//
//  FilteredVenuesCell.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 10/26/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import UIKit

class FilteredVenuesHeader: UICollectionReusableView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let showResultBtn = InfoViewController.createGradientButton(withTitle: "Показать результаты")
    
    func setupViews() {
        addSubview(showResultBtn)
        
        showResultBtn.snp.makeConstraints { (make) in
            make.left.equalTo(60)
            make.right.equalTo(-60)
            make.height.equalTo(40)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        showResultBtn.addGradient()
    }
}
