//
//  VenueExtraInfoCell.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 11/12/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import UIKit

class VenueExtraInfoCell: VenueFiltersCell {
    
    override var filters: [VenueFilter] {
        didSet {
        }
    }
    
    var venueExtraInfo = [VenueExtraInfo]() {
        didSet {
            selectionStyle = .none
            
            if venueExtraInfo.count > 0 {
                setupViews()
            }
        }
    }
    
    override func setupViews() {
        removeAllSubviews()
        
        addSubview(borderView)
        
        for (index, info) in venueExtraInfo.enumerated() {
            let isMarked = info.isMarked
            
            let label = UILabel()
            label.numberOfLines = 0
            
            let attributedString = NSMutableAttributedString(string: info.title! + "\n", attributes: [NSFontAttributeName: UIFont.boldSystemFont(ofSize: 13), NSForegroundColorAttributeName: isMarked ? UIColor.white : Const.Colors.navBar])
            attributedString.append(NSAttributedString(string: info.text!, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 13), NSForegroundColorAttributeName: isMarked ? UIColor.white : Const.Colors.navBar]))
            label.attributedText = attributedString
            
            let line = UIView()
            line.backgroundColor = Const.Colors.navBar
            
            let bgView = UIView()
            bgView.backgroundColor = isMarked ? UIColor.rgb(233, green: 150, blue: 255) : UIColor.clear
            
            let subviews = borderView.subviews
            
            borderView.addSubview(bgView)
            borderView.addSubview(label)
            borderView.addSubview(line)
            
            label.snp.makeConstraints({ (make) in
                if index == 0 {
                    make.top.equalTo(8)
                }
                else {
                    make.top.equalTo(subviews[subviews.count-1].snp.bottom).offset(8)
                }
                make.left.equalTo(8)
                make.right.equalTo(-8)
                
                if index == venueExtraInfo.count-1 {
                    make.bottom.equalTo(self).offset(-8)
                    
                    borderView.snp.makeConstraints { (make) in
                        make.top.equalTo(10)
                        make.left.equalTo(20)
                        make.right.equalTo(-20)
                        make.bottom.equalTo(label).offset(8)
                    }
                }
            })
            
            bgView.snp.makeConstraints({ (make) in
                make.top.left.equalTo(label).offset(-8)
                make.bottom.right.equalTo(label).offset(8)
            })
            
            if index != venueExtraInfo.count-1 {
                line.snp.makeConstraints({ (make) in
                    make.top.equalTo(bgView.snp.bottom)
                    make.left.right.equalTo(borderView)
                    make.height.equalTo(1)
                })
            }
        }
    }
}
