//
//  VenueLinksCell.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 10/23/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import UIKit
import SafariServices

class VenueLinksCell: VenueFiltersCell, SFSafariViewControllerDelegate {
    
    var links = [String]() {
        didSet {
            selectionStyle = .none
            
            if links.count > 0 {
                setupViews()
            }
        }
    }
    
    override func setupViews() {
        removeAllSubviews()
        
        addSubview(borderView)
        
        for (index, link) in links.enumerated() {
            let label = UILabel()
            label.numberOfLines = 0
            label.textColor = UIColor.rgb(11, green: 0, blue: 128)
            label.font =  UIFont.systemFont(ofSize: 13)
            label.text = link
            label.tag = index
            label.isUserInteractionEnabled = true
            let linkTap = UITapGestureRecognizer(target: self, action: #selector(linkTapped(sender:)))
            label.addGestureRecognizer(linkTap)
            
            let line = UIView()
            line.backgroundColor = Const.Colors.navBar
            
            borderView.addSubview(label)
            addSubview(line)
            
            let subviews = borderView.subviews
            
            label.snp.makeConstraints({ (make) in
                if index == 0 {
                    make.top.equalTo(6)
                }
                else {
                    make.top.equalTo(subviews[index-1].snp.bottom).offset(13)
                }
                make.left.equalTo(6)
                make.right.equalTo(-6)
                
                if index == links.count-1 {
                    make.bottom.equalTo(self).offset(-6)
                    
                    borderView.snp.makeConstraints { (make) in
                        make.top.equalTo(10)
                        make.left.equalTo(20)
                        make.right.equalTo(-20)
                        make.bottom.equalTo(label).offset(6)
                    }
                }
            })
            
            if index != links.count-1 {
                line.snp.makeConstraints({ (make) in
                    make.top.equalTo(label.snp.bottom).offset(6)
                    make.left.right.equalTo(borderView)
                    make.height.equalTo(1)
                })
            }
        }
    }
    
    func linkTapped(sender: UITapGestureRecognizer) {
        guard let tag = sender.view?.tag else {
            return
        }
        var link = links[tag]
        if link.contains("www.") {
            link = link.replacingOccurrences(of: "www.", with: "http://")
        }
        
        guard let url = URL(string: link) else {
            return
        }
        
        let canOpenURL = UIApplication.shared.canOpenURL(url)
        if !canOpenURL {
            return
        }
        
        if #available(iOS 9.0, *) {
            let svc = SFSafariViewController(url: url)
            svc.delegate = self
            guard let window = UIApplication.shared.keyWindow, let rootVC = window.rootViewController else {
                openSafari(url: url)
                return
            }
            UIApplication.shared.statusBarStyle = .default
            rootVC.present(svc, animated: true, completion: nil)
        } else {
            openSafari(url: url)
        }
    }
    
    func openSafari(url: URL) {
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    @available(iOS 9.0, *)
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    
}
