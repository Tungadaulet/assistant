//
//  SliderCell.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 10/20/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import UIKit

class SliderCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let imageView: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        return iv
    }()
    
    func setupViews() {
        addSubview(imageView)
        
        imageView.snp.makeConstraints { (make) in
            make.edges.equalTo(0)
        }
    }
    
}
