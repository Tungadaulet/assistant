//
//  IndicatorFooter.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 11/2/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import UIKit

class IndicatorFooter: UICollectionReusableView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let aiv = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    
    func setupViews() {
        addSubview(aiv)
        
        aiv.snp.makeConstraints { (make) in
            make.centerX.equalTo(self)
        }
    }
}
