//
//  VenueInfoCell.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 10/20/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import UIKit

class VenueInfoCell: UITableViewCell {
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = .none
        
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 15)
        label.textAlignment = .center
        label.textColor = Const.Colors.menu
        return label
    }()
    
    let topLine = VenueInfoCell.createSeparatorLine()
    let bottomLine = VenueInfoCell.createSeparatorLine()
    
    static func createSeparatorLine() -> UIView {
        let view = UIView()
        view.backgroundColor = UIColor.lightGray.withAlphaComponent(0.3)
        return view
    }
    
    let ratingLabel: UILabel = {
        let label = UILabel()
        label.textColor = Const.Colors.navBar        
        return label
    }()
    
    let ratingImageView = UIImageView(image: #imageLiteral(resourceName: "rating-1"))
    
    let reviewLabel: UILabel = {
        let label = UILabel()
        label.textColor = Const.Colors.navBar
        return label
    }()
    
    let workTimeView: UIView = {
        let view = UIView()
        view.layer.borderColor = Const.Colors.navBar.cgColor
        view.layer.borderWidth = 1
        view.layer.cornerRadius = 4
        return view
    }()
    
    let timeView: UIView = {
        let view = UIView()
        view.isUserInteractionEnabled = true
        let label = UILabel()
        label.textColor = .white
        label.text = "Время работы"
        label.font = UIFont.boldSystemFont(ofSize: 13)
        label.textAlignment = .center
        view.addSubview(label)
        label.isUserInteractionEnabled = true
        label.snp.makeConstraints({ (make) in
            make.edges.equalTo(0)
        })
        return view
    }()
    
    let workTimeLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = Const.Colors.navBar
        label.numberOfLines = 0
        label.isUserInteractionEnabled = true
        return label
    }()
    
    func setupViews() {
        addSubview(nameLabel)
        addSubview(topLine)
        addSubview(ratingLabel)
        addSubview(ratingImageView)
        addSubview(reviewLabel)
        addSubview(bottomLine)
        addSubview(workTimeView)
        addSubview(timeView)
        addSubview(workTimeLabel)
        
        nameLabel.snp.makeConstraints { (make) in
            make.top.equalTo(10)
            make.left.right.equalTo(0)
        }
        
        topLine.snp.makeConstraints { (make) in
            make.top.equalTo(nameLabel.snp.bottom).offset(10)
            make.left.right.equalTo(0)
            make.height.equalTo(1)
        }
        
        ratingLabel.snp.makeConstraints { (make) in
            make.top.equalTo(topLine.snp.bottom).offset(8)
            make.left.equalTo(20)
        }
        
        ratingImageView.snp.makeConstraints { (make) in
            make.centerY.equalTo(ratingLabel)
            make.left.equalTo(ratingLabel.snp.right).offset(6)
        }
        
        reviewLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(ratingLabel)
            make.left.equalTo(ratingImageView.snp.right).offset(10)
        }
        
        bottomLine.snp.makeConstraints { (make) in
            make.top.equalTo(ratingLabel.snp.bottom).offset(10)
            make.left.right.equalTo(0)
            make.height.equalTo(1)
        }
        
        workTimeView.snp.makeConstraints { (make) in
            make.top.equalTo(bottomLine.snp.bottom).offset(10)
            make.left.equalTo(20)
            make.right.equalTo(-20)
            make.height.equalTo(workTimeView.snp.width).multipliedBy(0.13)
            make.bottom.equalTo(0)
        }
        
        timeView.snp.makeConstraints { (make) in
            make.top.left.bottom.equalTo(workTimeView)
            make.right.equalTo(workTimeView.snp.centerX)
        }
        
        workTimeLabel.snp.makeConstraints { (make) in
            make.top.bottom.right.equalTo(workTimeView)
            make.left.equalTo(workTimeView.snp.centerX)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        timeView.roundCorners(corners: [.topLeft, .bottomLeft], radius: 4)
        timeView.addGradient()
    }
}
