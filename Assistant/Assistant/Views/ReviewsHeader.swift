//
//  ReviewsHeader.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 10/24/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import UIKit

class ReviewsHeader: UITableViewHeaderFooterView {
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let textView: UITextView = {
        let tv = UITextView()
        tv.isScrollEnabled = false
        tv.font = UIFont.systemFont(ofSize: 13)
        tv.textColor = Const.Colors.navBar
        tv.layer.borderColor = Const.Colors.navBar.cgColor
        tv.layer.borderWidth = 1
        tv.layer.cornerRadius = 6
        return tv
    }()
    
    let sendButton = InfoViewController.createGradientButton(withTitle: "Отправить")
    
    let reviewsCountLabel: UILabel = {
        let label = UILabel()
        label.textColor = Const.Colors.navBar
        return label
    }()
    
    func setupViews() {
        addSubview(textView)
        addSubview(sendButton)
        addSubview(reviewsCountLabel)
        
        textView.snp.makeConstraints { (make) in
            make.top.left.equalTo(12)
            make.right.equalTo(-12)
            make.height.greaterThanOrEqualTo(textView.snp.width).multipliedBy(0.18)
        }
        
        sendButton.snp.makeConstraints { (make) in
            make.top.equalTo(textView.snp.bottom).offset(12)
            make.centerX.equalTo(self)
            make.width.equalTo(self).multipliedBy(0.3)
            make.height.equalTo(40)
        }
        
        reviewsCountLabel.snp.makeConstraints { (make) in
            make.top.equalTo(sendButton.snp.bottom).offset(20)
            make.left.equalTo(30)
            make.bottom.equalTo(0)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        sendButton.addGradient()
    }
}


