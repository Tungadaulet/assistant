//
//  VenueMenuExpandedCell.swift
//  Assistant
//
//  Created by Daulet Tungatarov on 10/24/17.
//  Copyright © 2017 Daulet Tungatarov. All rights reserved.
//

import UIKit

class VenueMenuExpandedCell: ExpandedCell {
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setupSeparatorLine() {}
    
    let checkedImageView = UIImageView(image: #imageLiteral(resourceName: "unchecked-square"))
    
    let priceLabel: UILabel = {
        let label = UILabel()
        label.textColor = Const.Colors.navBar
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.textAlignment = .right
        return label
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = Const.Colors.navBar
        label.numberOfLines = 0
        return label
    }()
    
    override func configViews() {
        selectionStyle = .none
    }
    
    func setupViews() {
        addSubview(priceLabel)
        addSubview(checkedImageView)
        addSubview(titleLabel)
        
        priceLabel.snp.makeConstraints { (make) in
            make.right.equalTo(-15)
            make.centerY.equalTo(self)
        }
        
        checkedImageView.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.centerY.equalTo(self)
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(checkedImageView.snp.right).offset(12)
            make.centerY.equalTo(self)
            make.right.equalTo(priceLabel.snp.left).offset(-12)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if tag == 1 {
            self.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 4)
        }
        else {
            self.roundCorners(corners: [.allCorners], radius: 0)
        }
    }
    
    
    
}
